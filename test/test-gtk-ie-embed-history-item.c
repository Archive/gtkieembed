#include "ie-utils.h"
#include <gcutter.h>

#include "gtk-ie-embed-history-item.h"

void test_new (void);
void test_uri (void);
void test_title (void);

static GtkIEEmbedHistoryItem *item;

void
setup (void)
{
    item =  NULL;
}

void
teardown (void)
{
    if (item)
        g_object_unref(item);
}

void
test_new (void)
{
    item = gtk_ie_embed_history_item_new("URI", "Title");
    cut_assert(item);

    cut_assert_equal_string("URI", gtk_ie_embed_history_item_get_uri(item));
    cut_assert_equal_string("Title", gtk_ie_embed_history_item_get_title(item));
}

void
test_uri (void)
{
    cut_trace(test_new());

    gtk_ie_embed_history_item_set_uri(item, "New URI");
    cut_assert_equal_string("New URI", gtk_ie_embed_history_item_get_uri(item));
}

void
test_title (void)
{
    cut_trace(test_new());

    gtk_ie_embed_history_item_set_title(item, "New Title");
    cut_assert_equal_string("New Title", gtk_ie_embed_history_item_get_title(item));
}

/*
vi:ts=4:nowrap:ai:expandtab:sw=4
*/
