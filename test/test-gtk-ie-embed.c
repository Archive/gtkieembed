#include <gcutter.h>

#include "gtk-ie-embed.h"

void test_new (void);

static GtkWidget *ie;

void
setup (void)
{
    ie =  NULL;
}

void
teardown (void)
{
    if (ie)
        gtk_widget_destroy(ie);
}

void
test_new (void)
{
    ie = gtk_ie_embed_new();
    cut_assert(ie);
}

/*
vi:ts=4:nowrap:ai:expandtab:sw=4
*/
