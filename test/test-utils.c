#include "ie-utils.h"
#include <gcutter.h>

void test_convert (void);

static BSTR bstr;
static gchar *utf8_string;

void
setup (void)
{
    bstr = NULL;
    utf8_string = NULL;
}

void
teardown (void)
{
    SysFreeString (bstr);
    g_free (utf8_string);
}


void
test_convert (void)
{
    bstr = _ie_utils_utf8_to_BSTR ("ABC");

    utf8_string = _ie_utils_BSTR_to_utf8 (bstr);

    cut_assert_equal_string ("ABC", utf8_string);
}

/*
vi:ts=4:nowrap:ai:expandtab:sw=4
*/
