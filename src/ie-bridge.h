/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 *  Copyright (C) 2007 Hiroyuki Ikezoe  <poincare@ikezoe.net>
 *
 *  This library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __IE_BRIDGE_H__
#define __IE_BRIDGE_H__

#include <glib-object.h>
#include <gtk/gtkwidget.h>
#include "gtk-ie-embed.h"
#include <windows.h>

G_BEGIN_DECLS

#define TYPE_IE_BRIDGE            (_ie_bridge_get_type ())
#define IE_BRIDGE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_IE_BRIDGE, IEBridge))
#define IE_BRIDGE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_IE_BRIDGE, IEBridgeClass))
#define IS_IE_BRIDGE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_IE_BRIDGE))
#define IS_IE_BRIDGE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_IE_BRIDGE))
#define IE_BRIDGE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj), TYPE_IE_BRIDGE, IEBridgeClass))

typedef struct _IEBridge IEBridge;
typedef struct _IEBridgeClass IEBridgeClass;

#ifdef _WINDOWS
#include <exdisp.h>
#else
typedef enum RefreshConstants {
    REFRESH_NORMAL = 0,
    REFRESH_IFEXPIRED = 1,
    REFRESH_COMPLETELY = 3
} RefreshConstants;
#endif /* _WINDOWS */ 

struct _IEBridge
{
    GObject object;
};

struct _IEBridgeClass
{
    GObjectClass parent_class;
};

GType       _ie_bridge_get_type         (void) G_GNUC_CONST;

IEBridge   *_ie_bridge_new              (GtkWidget *widget);

void        _ie_bridge_set_visible      (IEBridge *ie,
                                         gboolean visible);
void        _ie_bridge_load_url         (IEBridge *ie,
                                         const gchar *url);
void        _ie_bridge_reload           (IEBridge *ie,
                                         RefreshConstants level);
void        _ie_bridge_stop             (IEBridge *ie);
gboolean    _ie_bridge_is_loading       (IEBridge *ie);
void        _ie_bridge_go_back          (IEBridge *ie);
void        _ie_bridge_go_forward       (IEBridge *ie);
void        _ie_bridge_go_relative_position
                                        (IEBridge *ie,
                                         gint offset);
gchar      *_ie_bridge_get_location     (IEBridge *ie);
gchar      *_ie_bridge_get_title        (IEBridge *ie);

gboolean    _ie_bridge_can_go_forward   (IEBridge *ie);
gboolean    _ie_bridge_can_go_back      (IEBridge *ie);
void        _ie_bridge_cut_clipboard    (IEBridge *ie);
void        _ie_bridge_copy_clipboard   (IEBridge *ie);
void        _ie_bridge_paste_clipboard  (IEBridge *ie);
void        _ie_bridge_select_all       (IEBridge *ie);
gboolean    _ie_bridge_can_cut_clipboard
                                        (IEBridge *ie);
gboolean    _ie_bridge_can_copy_clipboard
                                        (IEBridge *ie);
gboolean    _ie_bridge_can_paste_clipboard
                                        (IEBridge *ie);
void        _ie_bridge_set_font_size    (IEBridge *ie,
                                         gint size);
gint        _ie_bridge_get_font_size    (IEBridge *ie);
void        _ie_bridge_print            (IEBridge *ie);
void        _ie_bridge_page_setup       (IEBridge *ie);
void        _ie_bridge_print_preview    (IEBridge *ie);
gchar      *_ie_bridge_get_charset      (IEBridge *ie);
void        _ie_bridge_set_charset      (IEBridge *ie,
                                         const gchar *charset);
void        _ie_bridge_save             (IEBridge *ie,
                                         const gchar *filename);
gchar      *_ie_bridge_get_last_modified
                                        (IEBridge *ie);
gchar      *_ie_bridge_get_selected_text
                                        (IEBridge *ie);
gboolean    _ie_bridge_find_string      (IEBridge *ie,
                                         const gchar *string,
                                         gboolean forward,
                                         gboolean wrap);
void        _ie_bridge_load_html_from_string
                                        (IEBridge *ie,
                                         const gchar *string);
void        _ie_bridge_title_changed    (IEBridge *ie,
                                         BSTR title);
void        _ie_bridge_location_changed (IEBridge *ie,
                                         BSTR location);
void        _ie_bridge_status_text_change
                                        (IEBridge *ie,
                                         BSTR text);
void        _ie_bridge_connect_document_event_dispatcher
                                        (IEBridge *ie);
void        _ie_bridge_disconnect_document_event_dispatcher
                                        (IEBridge *ie);
void        _ie_bridge_set_forward_state
                                        (IEBridge *ie,
                                         gboolean enable);
void        _ie_bridge_set_backward_state
                                        (IEBridge *ie,
                                         gboolean enable);
gboolean    _ie_bridge_new_window       (IEBridge *ie,
                                         gpointer *newDispatch);
gboolean    _ie_bridge_close_window     (IEBridge *ie,
                                         gboolean is_child);
void        _ie_bridge_net_start        (IEBridge *ie);
void        _ie_bridge_net_stop         (IEBridge *ie);
void        _ie_bridge_progress_change  (IEBridge *ie,
                                         glong current_progress,
                                         glong max_progress);
gpointer    _ie_bridge_get_browser_object
                                        (IEBridge *ie);
gboolean    _ie_bridge_mouse_down       (IEBridge *ie);
gboolean    _ie_bridge_mouse_move       (IEBridge *ie);
gboolean    _ie_bridge_mouse_up         (IEBridge *ie);
gboolean    _ie_bridge_mouse_click      (IEBridge *ie);
gboolean    _ie_bridge_is_mapped        (IEBridge *ie);
void        _ie_bridge_ready            (IEBridge *ie);
gboolean    _ie_bridge_is_ready         (IEBridge *ie);
gboolean    _ie_bridge_get_use_context_menu
                                        (IEBridge *ie);
void        _ie_bridge_selection_changed
                                        (IEBridge *ie);
void        _ie_bridge_load_favicon     (IEBridge *ie);
GList      *_ie_bridge_get_history      (IEBridge *ie,
                                         GtkIEEmbedHistoryDirection direction);
void        _ie_bridge_set_history      (IEBridge *ie,
                                         GtkIEEmbedHistoryDirection direction,
                                         const GList *history);
void        _ie_bridge_set_whole_history
                                        (IEBridge *ie,
                                         const GList *history,
                                         guint current_position);
guint       _ie_bridge_get_history_count
                                        (IEBridge *ie);
guint       _ie_bridge_get_current_position_in_history
                                        (IEBridge *ie);
GtkIEEmbedHistoryItem *
            _ie_bridge_get_history_item_at_relative_position
                                        (IEBridge *ie,
                                         gint offset);
G_END_DECLS

#endif /* __IE_BRIDGE_H__ */

/*
vi:ts=4:nowrap:ai:expandtab:sw=4
*/

