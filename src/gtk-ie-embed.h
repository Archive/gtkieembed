/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 *  Copyright (C) 2007 Hiroyuki Ikezoe  <poincare@ikezoe.net>
 *
 *  This library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __GTK_IE_EMBED_H__
#define __GTK_IE_EMBED_H__

#include <gtk/gtk.h>
#include <gtk-ie-embed-history-item.h>

G_BEGIN_DECLS

#define GTK_TYPE_IE_EMBED            (gtk_ie_embed_get_type ())
#define GTK_IE_EMBED(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTK_TYPE_IE_EMBED, GtkIEEmbed))
#define GTK_IE_EMBED_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GTK_TYPE_IE_EMBED, GtkIEEmbedClass))
#define GTK_IS_IE_EMBED(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTK_TYPE_IE_EMBED))
#define GTK_IS_IE_EMBED_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GTK_TYPE_IE_EMBED))
#define GTK_IE_EMBED_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj), GTK_TYPE_IE_EMBED, GtkIEEmbedClass))

typedef struct _GtkIEEmbed GtkIEEmbed;
typedef struct _GtkIEEmbedClass GtkIEEmbedClass;

typedef enum {
    GTK_IE_EMBED_RELOAD_NORMAL     = 0,
    GTK_IE_EMBED_RELOAD_IFEXPIRED  = 1,
    GTK_IE_EMBED_RELOAD_COMPLETELY = 3
} GtkIEEmbedReloadFlag;

typedef enum {
    GTK_IE_EMBED_FONT_SMALLEST = 0,
    GTK_IE_EMBED_FONT_SMALL    = 1,
    GTK_IE_EMBED_FONT_MEDIUM   = 2,
    GTK_IE_EMBED_FONT_LARGE    = 3,
    GTK_IE_EMBED_FONT_LARGEST  = 4
} GtkIEEmbedFontSize;

typedef struct _GtkIEEmbedDOMEventTarget GtkIEEmbedDOMEventTarget;

typedef struct
{
    guint button;
    gboolean shift_key;
    gboolean control_key;
    gboolean alt_key;
    gboolean meta_key;

    gint x;
    gint y;

    GtkIEEmbedDOMEventTarget *target;
} GtkIEEmbedDOMMouseEvent;

typedef struct
{
    gchar *name;
    gchar *value;
} GtkIEEmbedDOMEventTargetAttribute;

struct _GtkIEEmbed
{
    GtkWidget parent;
};

struct _GtkIEEmbedClass
{
    GtkWidgetClass parent_class;

    void     (*location)            (GtkIEEmbed *ie, const gchar *location);
    void     (*title)               (GtkIEEmbed *ie, const gchar *title);
    void     (*progress)            (GtkIEEmbed *ie,
                                     glong       current_progress,
                                     glong       max_progress);
    void     (*net_start)           (GtkIEEmbed *ie);
    void     (*net_stop)            (GtkIEEmbed *ie);
    void     (*status_text)         (GtkIEEmbed *ie, const gchar *text);
    gboolean (*new_window)          (GtkIEEmbed *ie, GtkIEEmbed **newie);
    gboolean (*close_window)        (GtkIEEmbed *ie);
    gboolean (*dom_mouse_down)      (GtkIEEmbed *ie,
                                     GtkIEEmbedDOMMouseEvent *event);
    gboolean (*dom_mouse_move)      (GtkIEEmbed *ie,
                                     GtkIEEmbedDOMMouseEvent *event);
    gboolean (*dom_mouse_up)        (GtkIEEmbed *ie,
                                     GtkIEEmbedDOMMouseEvent *event);
    gboolean (*dom_mouse_click)     (GtkIEEmbed *ie,
                                     GtkIEEmbedDOMMouseEvent *event);
    void     (*selection_changed)   (GtkIEEmbed *ie);
    void     (*favicon)             (GtkIEEmbed *ie);
};

GType       gtk_ie_embed_get_type        (void) G_GNUC_CONST;

GtkWidget  *gtk_ie_embed_new             (void);

void        gtk_ie_embed_load_url        (GtkIEEmbed *ie, const gchar *url);
void        gtk_ie_embed_load_html_from_string
                                         (GtkIEEmbed *ie, const gchar *string);
void        gtk_ie_embed_reload          (GtkIEEmbed *ie,
                                          GtkIEEmbedReloadFlag flag);
void        gtk_ie_embed_stop            (GtkIEEmbed *ie);
gboolean    gtk_ie_embed_is_loading      (GtkIEEmbed *ie);
void        gtk_ie_embed_go_back         (GtkIEEmbed *ie);
void        gtk_ie_embed_go_forward      (GtkIEEmbed *ie);
void        gtk_ie_embed_go_relative_position
                                         (GtkIEEmbed *ie,
                                          gint offset);
gboolean    gtk_ie_embed_can_go_forward  (GtkIEEmbed *ie);
gboolean    gtk_ie_embed_can_go_back     (GtkIEEmbed *ie);
gchar      *gtk_ie_embed_get_location    (GtkIEEmbed *ie);
gchar      *gtk_ie_embed_get_title       (GtkIEEmbed *ie);
void        gtk_ie_embed_cut_clipboard   (GtkIEEmbed *ie);
void        gtk_ie_embed_copy_clipboard  (GtkIEEmbed *ie);
void        gtk_ie_embed_paste_clipboard (GtkIEEmbed *ie);
void        gtk_ie_embed_select_all      (GtkIEEmbed *ie);
gboolean    gtk_ie_embed_can_cut_clipboard
                                         (GtkIEEmbed *ie);
gboolean    gtk_ie_embed_can_copy_clipboard
                                         (GtkIEEmbed *ie);
gboolean    gtk_ie_embed_can_paste_clipboard
                                         (GtkIEEmbed *ie);
void        gtk_ie_embed_set_font_size   (GtkIEEmbed *ie,
                                          GtkIEEmbedFontSize size);
GtkIEEmbedFontSize
            gtk_ie_embed_get_font_size   (GtkIEEmbed *ie);
void        gtk_ie_embed_print           (GtkIEEmbed *ie);
void        gtk_ie_embed_page_setup      (GtkIEEmbed *ie);
void        gtk_ie_embed_print_preview   (GtkIEEmbed *ie);
gchar      *gtk_ie_embed_get_charset     (GtkIEEmbed *ie);
void        gtk_ie_embed_set_charset     (GtkIEEmbed *ie, const gchar *charset);
void        gtk_ie_embed_save_as         (GtkIEEmbed *ie, const gchar *filename);

/* return "MM/DD/YY hh:mm:ss" string */
gchar      *gtk_ie_embed_get_last_modified
                                         (GtkIEEmbed *ie);
gchar      *gtk_ie_embed_get_selected_text
                                         (GtkIEEmbed *ie);
gboolean    gtk_ie_embed_find_string     (GtkIEEmbed *ie,
                                          const gchar *string,
                                          gboolean forward,
                                          gboolean wrap);
void        gtk_ie_embed_set_use_context_menu
                                         (GtkIEEmbed *ie,
                                          gboolean use);
gboolean    gtk_ie_embed_get_use_context_menu
                                         (GtkIEEmbed *ie);
GList       *gtk_ie_embed_get_backward_history
                                         (GtkIEEmbed *ie);
GList       *gtk_ie_embed_get_forward_history
                                         (GtkIEEmbed *ie);
void         gtk_ie_embed_set_backward_history
                                         (GtkIEEmbed *ie,
                                          const GList *history);
void         gtk_ie_embed_set_forward_history
                                         (GtkIEEmbed *ie,
                                          const GList *history);
void         gtk_ie_embed_set_whole_history
                                         (GtkIEEmbed *ie,
                                          const GList *history,
                                          guint current_position);
guint        gtk_ie_embed_get_history_count
                                         (GtkIEEmbed *ie);
guint        gtk_ie_embed_get_current_position_in_history
                                         (GtkIEEmbed *ie);
GtkIEEmbedHistoryItem *
             gtk_ie_embed_get_history_item_at_relative_position
                                         (GtkIEEmbed *ie,
                                          gint position);
/* event target */
const gchar *gtk_ie_embed_dom_event_target_get_name
                                         (GtkIEEmbedDOMEventTarget *target);
const gchar *gtk_ie_embed_dom_event_target_get_attribute_value
                                         (GtkIEEmbedDOMEventTarget *target,
                                          const gchar *attribute_name);
const GList *gtk_ie_embed_dom_event_target_get_attributes
                                         (GtkIEEmbedDOMEventTarget *target);
void         gtk_ie_embed_dom_event_target_free
                                         (GtkIEEmbedDOMEventTarget *target);

G_END_DECLS

#endif /* __GTK_IE_EMBED_H__ */

/*
vi:ts=4:nowrap:ai:expandtab:sw=4
*/

