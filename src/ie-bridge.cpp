/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 *  Copyright (C) 2007 Hiroyuki Ikezoe  <poincare@ikezoe.net>
 *
 *  This library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif /* HAVE_CONFIG_H */

#include "ie-bridge.h"

#include <gdk/gdkwin32.h>
#include <windows.h>
#include <exdisp.h>
#include <unknwn.h>
#include <mshtml.h>
#include <servprov.h>
#ifdef HAVE_TLOGSTG_H
#include <tlogstg.h>
#endif

#include "gtk-ie-embed.h"
#include "gtk-ie-embed-private.h"
#include "gtk-ie-embed-history-item.h"
#include "ie-browser-event-dispatcher.h"
#include "ie-document-event-dispatcher.h"
#include "ie-utils.h"

enum {
    PROP_0,
    PROP_GTK_WIDGET
};

typedef struct _IEBridgePriv IEBridgePriv;
struct _IEBridgePriv
{
    GtkWidget *widget;
    gboolean can_go_forward;
    gboolean can_go_back;
    gboolean ready;
    gboolean is_setting_history;

    IWebBrowser2 *web_browser;
    IEBrowserEventDispatcher *browser_event_dispatcher;
    IEDocumentEventDispatcher *document_event_dispatcher;
    IHTMLDocument2 *current_document;
    DWORD browser_event_cookie;
    DWORD document_event_cookie;
#ifdef HAVE_TLOGSTG_H
    ITravelLogEntry *old_current_entry;
#endif
};

#define IE_BRIDGE_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), TYPE_IE_BRIDGE, IEBridgePriv))

G_DEFINE_TYPE (IEBridge, _ie_bridge, G_TYPE_OBJECT)

typedef HRESULT (__stdcall *ATLAXGETCONTROL) (HWND, IUnknown **);
static HMODULE hModule = NULL;
static ATLAXGETCONTROL AtlAxGetControl = NULL;

#ifdef HAVE_TLOGSTG_H
const GUID IID_ITravelLogStg = {0x7ebfdd80, 0xad18, 0x11d3, 0xa4,0xc5, 0x00,0xc0,0x4f,0x72,0xd6,0xb8};
#endif

/* virtual functions for GtkObject class */
static GObject *constructor        (GType                  type,
                                    guint                  n_props,
                                    GObjectConstructParam *props);
static void   dispose              (GObject               *object);
static void   set_property         (GObject               *object,
                                    guint                  prop_id,
                                    const GValue          *value,
                                    GParamSpec            *pspec);
static void   get_property         (GObject               *object,
                                    guint                  prop_id,
                                    GValue                *value,
                                    GParamSpec            *pspec);

static void
_ie_bridge_class_init (IEBridgeClass *klass)
{
    GObjectClass   *gobject_class = G_OBJECT_CLASS (klass);

    gobject_class->dispose      = dispose;
    gobject_class->constructor  = constructor;
    gobject_class->set_property = set_property;
    gobject_class->get_property = get_property;

    g_object_class_install_property (
        gobject_class,
        PROP_GTK_WIDGET,
        g_param_spec_object (
            "widget",
            "Parent Widget",
            "Parent GtkWidget",
            GTK_TYPE_WIDGET,
            (GParamFlags) (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY)));

    g_type_class_add_private (gobject_class, sizeof (IEBridgePriv));
}

typedef BOOL (__stdcall *ATLAXWININIT) (void);

static gboolean
atl_initialize (void)
{
    static gboolean atl_initialized = FALSE;

    if (!atl_initialized) {
        ATLAXWININIT AtlAxWinInit = NULL;
        hModule = LoadLibraryA ("atl.dll");
        if (!hModule)
            return FALSE;
        CoInitialize (NULL);
        AtlAxWinInit = (ATLAXWININIT) GetProcAddress (hModule, "AtlAxWinInit");
        if (!AtlAxWinInit)
            return FALSE;
        AtlAxWinInit ();

        AtlAxGetControl = (ATLAXGETCONTROL) GetProcAddress (hModule, "AtlAxGetControl");

        atl_initialized = TRUE;
    }
    return TRUE;
}

static void
connect_browser_event_dispatcher (IEBridge *bridge)
{
    IConnectionPointContainer *iCPC;
    IConnectionPoint *iCP;
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (bridge);

    priv->web_browser->QueryInterface (IID_IConnectionPointContainer, (void **) &iCPC);
    iCPC->FindConnectionPoint (DIID_DWebBrowserEvents2, &iCP);
    priv->browser_event_dispatcher = new IEBrowserEventDispatcher (bridge);
    iCP->Advise (priv->browser_event_dispatcher, &priv->browser_event_cookie);
    iCPC->Release ();
    iCP->Release ();
}

static void
disconnect_browser_event_dispatcher (IEBridge *bridge)
{
    IConnectionPointContainer *iCPC;
    IConnectionPoint *iCP;
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (bridge);

    priv->web_browser->QueryInterface (IID_IConnectionPointContainer, (void **) &iCPC);
    iCPC->FindConnectionPoint (DIID_DWebBrowserEvents2, &iCP);
    iCP->Unadvise (priv->browser_event_cookie);
    iCPC->Release ();
    iCP->Release ();
}

static GObject *
constructor (GType type, guint n_props, GObjectConstructParam *props)
{
    GObject *object;
    GObjectClass *klass = G_OBJECT_CLASS (_ie_bridge_parent_class);
    IEBridgePriv *priv;
    HWND hwnd, hparent;
    HINSTANCE hInstance;

    object = klass->constructor (type, n_props, props);

    atl_initialize ();

    priv = IE_BRIDGE_GET_PRIVATE (object);
    hInstance = GetModuleHandle (NULL);
    hparent = (HWND) GDK_WINDOW_HWND (gtk_widget_get_parent_window (priv->widget));
    hwnd = CreateWindowA ("AtlAxWin", "Shell.Explorer.2", WS_CHILD | WS_VISIBLE,
                          priv->widget->allocation.x, priv->widget->allocation.y,
                          priv->widget->allocation.width, priv->widget->allocation.height,
                          hparent, (HMENU)0, hInstance, NULL);
    if (hwnd) {
        IUnknown *iUnknown;

        priv->widget->window = gdk_window_foreign_new ((GdkNativeWindow) hwnd);
        ShowWindow (hwnd, SW_HIDE);

        if (AtlAxGetControl (hwnd, &iUnknown) == S_OK) {
            iUnknown->QueryInterface (IID_IWebBrowser2, (void **) &priv->web_browser);
            priv->web_browser->put_RegisterAsBrowser (VARIANT_TRUE);
            priv->web_browser->put_Silent (VARIANT_TRUE);
            priv->web_browser->put_AddressBar (VARIANT_FALSE);
            priv->web_browser->put_StatusBar (VARIANT_FALSE);
            priv->web_browser->put_ToolBar (VARIANT_FALSE);
            priv->web_browser->put_FullScreen (VARIANT_FALSE);
            priv->web_browser->put_TheaterMode (VARIANT_FALSE);
            iUnknown->Release ();
        }
        connect_browser_event_dispatcher (IE_BRIDGE (object));
    }

    return object;
}

static void
_ie_bridge_init (IEBridge *ie)
{
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);

    priv->widget = NULL;
    priv->web_browser = NULL;
    priv->current_document = NULL;
    priv->browser_event_dispatcher = NULL;
    priv->browser_event_cookie = 0;
    priv->document_event_cookie = 0;
    priv->ready = FALSE;
    priv->is_setting_history = FALSE;

#ifdef HAVE_TLOGSTG_H
    priv->old_current_entry = NULL;
#endif
    priv->document_event_dispatcher = new IEDocumentEventDispatcher (ie);
}

static void
dispose (GObject *object)
{
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (object);

    if (priv->browser_event_dispatcher) {
        disconnect_browser_event_dispatcher (IE_BRIDGE (object));
        delete priv->browser_event_dispatcher;
        priv->browser_event_dispatcher = NULL;
    }

#ifdef HAVE_TLOGSTG_H
    if (priv->old_current_entry) {
        priv->old_current_entry->Release ();
        priv->old_current_entry = NULL;
    }
#endif

    if (priv->document_event_cookie) {
        _ie_bridge_disconnect_document_event_dispatcher (IE_BRIDGE (object));
    }

    if (priv->document_event_dispatcher) {
        delete priv->document_event_dispatcher;
        priv->document_event_dispatcher = NULL;
    }

    if (priv->web_browser) {
        priv->web_browser->Release ();
        priv->web_browser = NULL;
    }

    if (priv->widget) {
        g_object_unref (priv->widget);
        priv->widget = NULL;
    }

    if (G_OBJECT_CLASS (_ie_bridge_parent_class)->dispose)
        G_OBJECT_CLASS (_ie_bridge_parent_class)->dispose (object);
}

static void
set_property (GObject *object,
              guint prop_id,
              const GValue *value,
              GParamSpec *pspec)
{
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (object);

    switch (prop_id) {
    case PROP_GTK_WIDGET:
    {
        GObject *obj = G_OBJECT (g_value_get_object (value));
        priv->widget = GTK_WIDGET (g_object_ref (obj));
        break;
    }
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
get_property (GObject *object,
              guint prop_id,
              GValue *value,
              GParamSpec *pspec)
{
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (object);

    switch (prop_id) {
    case PROP_GTK_WIDGET:
        g_value_set_object (value, G_OBJECT (priv->widget));
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

IEBridge *
_ie_bridge_new (GtkWidget *widget)
{
    return IE_BRIDGE (g_object_new (TYPE_IE_BRIDGE,
                                    "widget", widget,
                                    NULL));
}

void
_ie_bridge_set_visible (IEBridge *ie, gboolean visible)
{
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);

    if (priv->web_browser) {
        VARIANT_BOOL B = visible ? VARIANT_TRUE : VARIANT_FALSE;
        priv->web_browser->put_Visible (B);
    }
}

void
_ie_bridge_load_url (IEBridge *ie, const gchar *url)
{
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);
    BSTR bstrUrl;

    if (!priv->web_browser)
        return;

    bstrUrl = _ie_utils_utf8_to_BSTR (url);
    if (bstrUrl) {
        priv->web_browser->Navigate (bstrUrl, NULL, NULL, NULL, NULL);
        SysFreeString (bstrUrl);
    }
}

void
_ie_bridge_reload (IEBridge *ie, RefreshConstants flag)
{
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);

    if (priv->web_browser) {
        VARIANT level;
        level.vt = VT_I4;
        level.intVal = flag;
        priv->web_browser->Refresh2 (&level);
    }
}

void
_ie_bridge_stop (IEBridge *ie)
{
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);

    if (priv->web_browser) {
        priv->web_browser->Stop ();
    }
}

gboolean
_ie_bridge_is_loading (IEBridge *ie)
{
    VARIANT_BOOL vB;
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);

    if (!priv->web_browser)
        return FALSE;

    priv->web_browser->get_Busy (&vB);
    return vB == VARIANT_TRUE ? TRUE : FALSE;
}

void
_ie_bridge_go_back (IEBridge *ie)
{
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);

    if (priv->web_browser) {
        priv->web_browser->GoBack ();
    }
}

void
_ie_bridge_go_forward (IEBridge *ie)
{
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);

    if (priv->web_browser) {
        priv->web_browser->GoForward ();
    }
}

gchar *
_ie_bridge_get_location (IEBridge *ie)
{
    gchar *location = NULL;

    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);

    if (priv->web_browser) {
        BSTR URL;
        priv->web_browser->get_LocationURL (&URL);
        if (URL) {
            location = _ie_utils_BSTR_to_utf8 (URL);
            SysFreeString (URL);
        }
    }
    return location;
}

gchar *
_ie_bridge_get_title (IEBridge *ie)
{
    gchar *title = NULL;

    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);

    if (priv->web_browser) {
        BSTR Name;
        priv->web_browser->get_LocationName (&Name);
        if (Name) {
            title = _ie_utils_BSTR_to_utf8 (Name);
            SysFreeString (Name);
        }
    }
    return title;
}

gchar *
_ie_bridge_get_charset (IEBridge *ie)
{
    gchar *charset = NULL;

    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);

    if (priv->web_browser) {
        BSTR Charset = NULL;
        priv->current_document->get_charset (&Charset);
        if (Charset) {
            charset = _ie_utils_BSTR_to_utf8 (Charset);
            SysFreeString (Charset);
        }
    }
    return charset;
}

void
_ie_bridge_set_charset (IEBridge *ie, const gchar *charset)
{
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);

    if (priv->web_browser) {
        BSTR Charset;
        Charset = _ie_utils_utf8_to_BSTR (charset);
        if (Charset) {
            priv->current_document->put_charset (Charset);
            SysFreeString (Charset);
        }
    }
}

gboolean
_ie_bridge_can_go_forward (IEBridge *ie)
{
    return IE_BRIDGE_GET_PRIVATE (ie)->can_go_forward;
}

gboolean
_ie_bridge_can_go_back (IEBridge *ie)
{
    return IE_BRIDGE_GET_PRIVATE (ie)->can_go_back;
}

static void
_ie_bridge_exec_default_command (IEBridge *ie, OLECMDID cmd_id)
{
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);

    if (priv->web_browser) {
        priv->web_browser->ExecWB (cmd_id, OLECMDEXECOPT_DODEFAULT,
                                   NULL, NULL);
    }
}

void
_ie_bridge_cut_clipboard (IEBridge *ie)
{
    _ie_bridge_exec_default_command (ie, OLECMDID_CUT);
}

void
_ie_bridge_copy_clipboard (IEBridge *ie)
{
    _ie_bridge_exec_default_command (ie, OLECMDID_COPY);
}

void
_ie_bridge_paste_clipboard (IEBridge *ie)
{
    _ie_bridge_exec_default_command (ie, OLECMDID_PASTE);
}

void
_ie_bridge_select_all (IEBridge *ie)
{
    _ie_bridge_exec_default_command (ie, OLECMDID_SELECTALL);
}

static gboolean
_ie_bridge_is_enable_command (IEBridge *ie, OLECMDID cmd_id)
{
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);
    OLECMDF cmdF;

    if (!priv->web_browser)
        return FALSE;

    priv->web_browser->QueryStatusWB (cmd_id, &cmdF);
    return (cmdF & OLECMDF_ENABLED);
}

gboolean
_ie_bridge_can_cut_clipboard (IEBridge *ie)
{
    return _ie_bridge_is_enable_command (ie, OLECMDID_CUT);
}

gboolean
_ie_bridge_can_copy_clipboard (IEBridge *ie)
{
    return _ie_bridge_is_enable_command (ie, OLECMDID_COPY);
}

gboolean
_ie_bridge_can_paste_clipboard (IEBridge *ie)
{
    return _ie_bridge_is_enable_command (ie, OLECMDID_PASTE);
}

void
_ie_bridge_set_font_size (IEBridge *ie, gint size)
{
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);

    if (priv->web_browser) {
        VARIANT zoom;
        zoom.vt = VT_I4;
        zoom.lVal = size;
        priv->web_browser->ExecWB (OLECMDID_ZOOM,
                                   OLECMDEXECOPT_DODEFAULT,
                                   &zoom, NULL);
    }
}

gint
_ie_bridge_get_font_size (IEBridge *ie)
{
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);

    if (priv->web_browser) {
        VARIANT zoom;
        zoom.vt = VT_I4;
        priv->web_browser->ExecWB (OLECMDID_ZOOM,
                                   OLECMDEXECOPT_DODEFAULT,
                                   NULL, &zoom);
        return zoom.lVal;
    }
    return -1;
}

void
_ie_bridge_page_setup (IEBridge *ie)
{
    _ie_bridge_exec_default_command (ie, OLECMDID_PAGESETUP);
}

void
_ie_bridge_print (IEBridge *ie)
{
    _ie_bridge_exec_default_command (ie, OLECMDID_PRINT);
}

void
_ie_bridge_print_preview (IEBridge *ie)
{
    _ie_bridge_exec_default_command (ie, OLECMDID_PRINTPREVIEW);
}

void
_ie_bridge_save (IEBridge *ie, const gchar *filename)
{
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);
    IDispatch *doc = NULL;
    IPersistStreamInit *stream = NULL;
    IStream *file_stream = NULL;

    if (!priv->web_browser)
        return;

    priv->web_browser->get_Document (&doc);
    if (!doc)
        return;
    doc->QueryInterface (IID_IPersistStreamInit, (void **) &stream);
    if (!stream)
        return;
    stream->InitNew ();
    stream->Save (file_stream, VARIANT_TRUE);
    stream->Release ();
}

gchar *
_ie_bridge_get_last_modified (IEBridge *ie)
{
    BSTR mTime = NULL;
    gchar *last_modified = NULL;
    IHTMLDocument2 *doc = NULL;

    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);

    if (!priv->web_browser)
        return NULL;

    priv->web_browser->get_Document ((IDispatch **) &doc);
    if (!doc)
        return NULL;
    doc->get_lastModified (&mTime);
    if (mTime) {
        last_modified = _ie_utils_BSTR_to_utf8 (mTime);
        SysFreeString (mTime);
    }

    return last_modified;
}

static gboolean
get_selected_text_range (IHTMLDocument2 *document, IHTMLTxtRange **text_range)
{
    IHTMLSelectionObject *selection;
    gboolean ret = FALSE;

    if (!document)
        return FALSE;

    document->get_selection (&selection);
    if (selection) {
        IDispatch *range_dispatch;
        selection->createRange (&range_dispatch);

        if (range_dispatch) {
            range_dispatch->QueryInterface (IID_IHTMLTxtRange, (void **) text_range);
            if (text_range)
                ret = TRUE;
            range_dispatch->Release ();
        }
        selection->Release ();
    }

    return ret;
}

gchar *
_ie_bridge_get_selected_text (IEBridge *ie)
{
    gchar *text = NULL;
    IHTMLTxtRange *text_range = NULL;
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);

    if (!priv->web_browser)
        return NULL;
    if (!priv->current_document)
        return NULL;
    if (get_selected_text_range (priv->current_document, &text_range) &&
        text_range) {
        BSTR selectedText = NULL;
        text_range->get_text (&selectedText);
        if (selectedText) {
            text = _ie_utils_BSTR_to_utf8 (selectedText);
            SysFreeString (selectedText);
        }
        text_range->Release();
    }

    return text;
}

gboolean
_ie_bridge_find_string (IEBridge *ie,
                        const gchar *string,
                        gboolean forward,
                        gboolean wrap)
{
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);
    IHTMLDocument2 *doc = NULL;
    IHTMLTxtRange *text_range = NULL, *selected_range = NULL;
    IHTMLElement *element = NULL;
    IHTMLBodyElement *body = NULL;
    gboolean ret = FALSE;

    if (!priv->web_browser)
        return FALSE;

    priv->web_browser->get_Document ((IDispatch **) &doc);
    if (!doc)
        return FALSE;

    doc->get_body (&element);
    if (!element) {
        doc->Release ();
        return FALSE;
    }
    element->QueryInterface (IID_IHTMLBodyElement, (void **) &body);
    if (body) {
        body->createTextRange (&text_range);
    }

    if (get_selected_text_range (doc, &selected_range) && selected_range) {
        long ActualCount;
        gunichar2 *unit, *how;
        BSTR Unit = NULL, How = NULL;
        Unit = _ie_utils_utf8_to_BSTR ("character");
        if (forward) {
            How = _ie_utils_utf8_to_BSTR ("EndToEnd");
            selected_range->moveStart (Unit, 1, &ActualCount);
        } else {
            How = _ie_utils_utf8_to_BSTR ("StartToStart");
            selected_range->moveEnd (Unit, -1, &ActualCount);
        }
        selected_range->setEndPoint (How, text_range);
        text_range->Release ();
        text_range = selected_range;
        SysFreeString (Unit);
        SysFreeString (How);
    }

    if (text_range) {
        BSTR bstr = NULL;
        bstr = _ie_utils_utf8_to_BSTR (string);
        if (bstr) {
            VARIANT_BOOL vBool;
            gint direction = forward ? 1 : -1;
            text_range->findText (bstr, direction, 0, &vBool);
            if (vBool == VARIANT_TRUE) {
                text_range->select ();
                ret = TRUE;
            } else if (selected_range && wrap) {
                text_range->Release ();
                body->createTextRange (&text_range);    
                text_range->findText (bstr, direction, 0, &vBool);
                if (vBool == VARIANT_TRUE) {
                    text_range->select ();
                    ret = TRUE;
                }
            }
            SysFreeString (bstr);
        }
        text_range->Release ();
    }

    if (body)
        body->Release ();
    element->Release ();

    return ret;
}

void
_ie_bridge_load_html_from_string (IEBridge *ie, const gchar *string)
{
    IHTMLDocument2 *doc = NULL;
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);
    BSTR bstr = NULL;
    VARIANT *param;
    SAFEARRAY *sfArray;

    if (!priv->web_browser)
        return;

    _ie_bridge_load_url (ie, "about:blank");
    priv->web_browser->get_Document ((IDispatch **) &doc);
    if (!doc)
        return;

    bstr = _ie_utils_utf8_to_BSTR (string);
    if (!bstr)
        return;

    sfArray = SafeArrayCreateVector (VT_VARIANT, 0, 1);
    if (!sfArray) {
        SysFreeString (bstr);
        return;
    }

    SafeArrayAccessData (sfArray, (LPVOID*) & param);
    param->vt = VT_BSTR;
    param->bstrVal = bstr;
    SafeArrayUnaccessData (sfArray);
    doc->write (sfArray);

    SysFreeString (bstr);
    SafeArrayDestroy (sfArray);
}

void
_ie_bridge_title_changed (IEBridge *ie, BSTR title)
{
    gchar *utf8_title;

    utf8_title = _ie_utils_BSTR_to_utf8 (title);
    if (utf8_title) {
        IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);
        g_signal_emit_by_name (priv->widget, "title", utf8_title);
        g_free (utf8_title);
    }
}

void
_ie_bridge_location_changed (IEBridge *ie, BSTR location)
{
    gchar *utf8_location;

    utf8_location = _ie_utils_BSTR_to_utf8 (location);
    if (utf8_location) {
        IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);
        g_signal_emit_by_name (priv->widget, "location", utf8_location);
        g_free (utf8_location);
    }
}

void
_ie_bridge_status_text_change (IEBridge *ie, BSTR text)
{
    gchar *utf8_text;

    utf8_text = _ie_utils_BSTR_to_utf8 (text);
    if (utf8_text) {
        IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);
        g_signal_emit_by_name (priv->widget, "status-text", utf8_text);
        g_free (utf8_text);
    }
}

void
_ie_bridge_connect_document_event_dispatcher (IEBridge *ie)
{
    IConnectionPointContainer *iCPC;
    IConnectionPoint *iCP;
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);

    if (priv->current_document)
        priv->current_document->Release ();
    priv->web_browser->get_Document ((IDispatch **) &priv->current_document);
    priv->current_document->QueryInterface (IID_IConnectionPointContainer, (void **) &iCPC);
    iCPC->FindConnectionPoint (DIID_HTMLDocumentEvents, &iCP);
    iCP->Advise (priv->document_event_dispatcher, &priv->document_event_cookie);
    iCPC->Release ();
    iCP->Release ();
}

void
_ie_bridge_disconnect_document_event_dispatcher (IEBridge *ie)
{
    IConnectionPointContainer *iCPC;
    IConnectionPoint *iCP;
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);

    if (!priv->current_document)
        return;
    priv->current_document->QueryInterface (IID_IConnectionPointContainer, (void **) &iCPC);
    iCPC->FindConnectionPoint (DIID_HTMLDocumentEvents, &iCP);
    iCP->Unadvise (priv->document_event_cookie);
    iCPC->Release ();
    iCP->Release ();
    priv->current_document->Release ();
    priv->current_document = NULL;
    priv->document_event_dispatcher->Release ();
    priv->document_event_cookie = 0;
}

void
_ie_bridge_set_forward_state (IEBridge *ie, gboolean enable)
{
    IE_BRIDGE_GET_PRIVATE (ie)->can_go_forward = enable;
}

void
_ie_bridge_set_backward_state (IEBridge *ie, gboolean enable)
{
    IE_BRIDGE_GET_PRIVATE (ie)->can_go_back = enable;
}

gboolean
_ie_bridge_new_window (IEBridge *ie, gpointer *newDispatch)
{
    gboolean cancel = FALSE;
    IEBridge *newbridge = NULL;
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);

    cancel = _gtk_ie_embed_new_window (GTK_IE_EMBED (priv->widget), &newbridge);

    if (newbridge) {
        IEBridgePriv *newpriv = IE_BRIDGE_GET_PRIVATE (newbridge);
        IDispatch *dispatch = NULL;
        if (newpriv->web_browser->get_Application (&dispatch) == S_OK) {
            *newDispatch = dispatch;
        }
    }
    return cancel;
}

gboolean
_ie_bridge_close_window (IEBridge *ie, gboolean is_child)
{
    gboolean cancel = FALSE;
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);

    g_signal_emit_by_name (priv->widget, "close-window",
                           &cancel);
    return cancel;
}

void
_ie_bridge_net_start (IEBridge *ie)
{
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);

    g_signal_emit_by_name (priv->widget, "net-start");
}

void
_ie_bridge_net_stop (IEBridge *ie)
{
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);

    g_signal_emit_by_name (priv->widget, "net-stop");
}

void
_ie_bridge_progress_change (IEBridge *ie, glong current_progress, glong max_progress)
{
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);

    g_signal_emit_by_name (priv->widget, "progress",
                           current_progress, max_progress);

}

void
_ie_bridge_ready (IEBridge *ie)
{
    IE_BRIDGE_GET_PRIVATE (ie)->ready = TRUE;
}

gboolean
_ie_bridge_is_ready (IEBridge *ie)
{
    return IE_BRIDGE_GET_PRIVATE (ie)->ready;
}

gpointer
_ie_bridge_get_browser_object (IEBridge *ie)
{
    return IE_BRIDGE_GET_PRIVATE (ie)->web_browser;
}

static void
_set_dom_event_target_attributes (GtkIEEmbedDOMEventTarget *target, IHTMLElement *target_element)
{
    GtkIEEmbedDOMEventTargetAttribute *attr = NULL;
#if defined(HAVE_IHTMLNODE) && defined (HAVE_IHTMLAttributeCollection)
    IHTMLDOMNode *node = NULL;
    IDispatch *attributes = NULL;
    IHTMLAttributeCollection *collection = NULL;
    target_element->QueryInterface (IID_IHTMLDOMNode, (IDispatch **) &node);
    node->get_attributes (&attributes);
    if (attributes) {
        long i, length;
        attributes->QueryInterface (IID_IHTMLAttributeCollection, (IHTMLAttributeCollection **) &collection);
        collection->get_length (&length);
        for (i = 0; i < length; i++) {
            IDispatch *disp = NULL;
            IHTMLDOMAttribute *dom_attr = NULL;
            BSTR nodeName;
            VARIANT nodeValue;
            gchar *utf8_name, *utf8_value;
            VARIANT v;
            v.vt = VT_I4;
            v.intVal = i;
            collection->get_item (&v, (IDispatch **) disp);
            disp->QueryInterface (IID_IHTMLDOMAttribute, (IHTMLDOMAttribute **) &dom_attr);
            dom_attr->get_nodeName (&nodeName);
            dom_attr->get_nodeValue (&nodeValue);
            utf8_name = _ie_utils_BSTR_to_utf8 (nodeName);
            if (nodeValue.vt == VT_BSTR && nodeValue.bstrVal)
                utf8_value = _ie_utils_BSTR_to_utf8 (nodeValue.bstrVal);

            attr = g_slice_new0 (GtkIEEmbedDOMEventTargetAttribute);
            attr->name = utf8_name;
            attr->value = utf8_value;
            target_attribute_list = g_list_prepend (target_attribute_list, attr);
            dom_attr->Release ();
            disp->Release ();
            SysFreeString (nodeName);
        }
        collection->Release ();
    }
    node->Release ();
#else
    /* workaround */
    VARIANT attrValue;
    BSTR attrName = NULL;

    attrName = _ie_utils_utf8_to_BSTR ("href");
    if (!attrName)
        return;

    target_element->getAttribute (attrName, 0, &attrValue);
    if (attrValue.vt == VT_BSTR && attrValue.bstrVal) {
        gchar *value = NULL;
        attr = g_slice_new0 (GtkIEEmbedDOMEventTargetAttribute);
        attr->name = g_strdup ("href");
        value = _ie_utils_BSTR_to_utf8 (attrValue.bstrVal);
        if (value)
            attr->value = value;
        target->attribute_list = g_list_prepend (target->attribute_list, attr);
    }
    SysFreeString (attrName);
#endif /* HAVE_IHTMLNODE */
}

static GtkIEEmbedDOMEventTarget *
_ie_bridge_create_dom_event_target (IHTMLElement *target_element)
{
    GtkIEEmbedDOMEventTarget *target;
    BSTR tagName = NULL;

    target = g_slice_new0 (GtkIEEmbedDOMEventTarget);

    target_element->get_tagName (&tagName);
    if (tagName) {
        gchar *utf8_tag_name;
        utf8_tag_name = _ie_utils_BSTR_to_utf8 (tagName);
        target->name = utf8_tag_name;
        SysFreeString (tagName);
    }

    _set_dom_event_target_attributes (target, target_element);

    return target;
}

static GtkIEEmbedDOMMouseEvent *
_ie_bridge_create_dom_mouse_event (IEBridge *ie)
{
    gint x, y;
    GdkModifierType mask;
    GtkIEEmbedDOMMouseEvent *event;
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);

    gdk_window_get_pointer (priv->widget->window, &x, &y, &mask);

    event = g_slice_new0 (GtkIEEmbedDOMMouseEvent);

    event->x = x;
    event->y = y;
    event->shift_key = mask & GDK_SHIFT_MASK;
    event->control_key = mask & GDK_CONTROL_MASK;
    event->alt_key = mask & GDK_MOD1_MASK;
    event->meta_key = mask & GDK_META_MASK;
    if (mask & GDK_BUTTON1_MASK)
        event->button = 0;
    else if (mask & GDK_BUTTON2_MASK)
        event->button = 1;
    else if (mask & GDK_BUTTON3_MASK)
        event->button = 2;

    if (priv->current_document) {
        IHTMLElement *element = NULL;

        priv->current_document->elementFromPoint (x, y, &element);
        if (element) {
            event->target = _ie_bridge_create_dom_event_target (element);
            element->Release ();
        }
    }

    return event;
}

static void
_ie_bridge_dom_mouse_event_free (GtkIEEmbedDOMMouseEvent *event)
{
    if (!event)
        return;

    if (event->target)
        gtk_ie_embed_dom_event_target_free (event->target);

    g_slice_free (GtkIEEmbedDOMMouseEvent, event);
}

static gboolean
_ie_bridge_emit_mouse_event (IEBridge *ie, const gchar *signal_name)
{
    GtkIEEmbedDOMMouseEvent *event;
    gboolean ret;
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);

    event = _ie_bridge_create_dom_mouse_event (ie);
    g_signal_emit_by_name (priv->widget, signal_name,
                           event, &ret);
    _ie_bridge_dom_mouse_event_free (event);

    return ret;
}

gboolean
_ie_bridge_mouse_down (IEBridge *ie)
{
    return _ie_bridge_emit_mouse_event (ie, "dom-mouse-down");
}

gboolean
_ie_bridge_mouse_move (IEBridge *ie)
{
    return _ie_bridge_emit_mouse_event (ie, "dom-mouse-move");
}

gboolean
_ie_bridge_mouse_up (IEBridge *ie)
{
    return _ie_bridge_emit_mouse_event (ie, "dom-mouse-up");
}

gboolean
_ie_bridge_mouse_click (IEBridge *ie)
{
    return _ie_bridge_emit_mouse_event (ie, "dom-mouse-click");
}

gboolean
_ie_bridge_is_mapped (IEBridge *ie)
{
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);
    return GTK_WIDGET_MAPPED (priv->widget);
}

gboolean
_ie_bridge_get_use_context_menu (IEBridge *ie)
{
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);
    return gtk_ie_embed_get_use_context_menu (GTK_IE_EMBED (priv->widget));
}

void
_ie_bridge_selection_changed (IEBridge *ie)
{
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);
    g_signal_emit_by_name (priv->widget, "selection-changed");
}

void
_ie_bridge_load_favicon (IEBridge *ie)
{
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);
    IHTMLElementCollection *collection;
    long i, length;

    if (!priv->web_browser)
        return;
    if (!priv->current_document)
        return;

    priv->current_document->get_links (&collection);
    if (!collection)
        return;

    collection->get_length (&length);
    for (i = 0; i < length; i++) {
        IDispatch *disp = NULL;
        IHTMLLinkElement *link_element = NULL;
        BSTR relName = NULL;
        gchar *utf8_name;
        VARIANT index;
        index.vt = VT_I4;
        index.intVal = i;
        collection->item (index, index, (IDispatch **) disp);
        disp->QueryInterface (IID_IHTMLLinkElement, (void **) &link_element);
        if (!link_element)
            continue;
        link_element->get_rel (&relName);
        if (relName) {
            utf8_name = _ie_utils_BSTR_to_utf8 (relName);
            if (!g_ascii_strcasecmp (utf8_name, "shortcut icon") ||
                !g_ascii_strcasecmp (utf8_name, "icon")) {
                BSTR href = NULL;
                link_element->get_href (&href);
                if (href) {
                    gchar *utf8_href;
                    utf8_href = _ie_utils_BSTR_to_utf8 (href);
                    /* load a favicon */
                    SysFreeString (href);
                    g_free (utf8_href);
                }
            }
            SysFreeString (relName);
            g_free (utf8_name);
        }
        disp->Release ();
        link_element->Release ();
    }
    collection->Release ();
}

#ifdef HAVE_TLOGSTG_H
static ITravelLogStg *
_get_travel_log (IEBridge *ie)
{
    IServiceProvider *service_provider = NULL;    
    ITravelLogStg *travel_log = NULL;
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);

    if (!priv->web_browser)
        return NULL;

    priv->web_browser->QueryInterface (IID_IServiceProvider, (void**) &service_provider);
    if (!service_provider)
        return NULL;

    service_provider->QueryService (SID_STravelLogCursor, IID_ITravelLogStg, (void**) &travel_log);
    return travel_log;
}
#endif

#ifdef HAVE_TLOGSTG_H
static GtkIEEmbedHistoryItem *
travel_log_entry_to_history_item (ITravelLogEntry *entry)
{
    LPOLESTR szURL, szTitle;
    gchar *uri, *title;
    GtkIEEmbedHistoryItem *history_item;

    entry->GetURL (&szURL);
    entry->GetTitle (&szTitle);

    uri = g_utf16_to_utf8 ((gunichar2*) szURL, -1, NULL, NULL, NULL);
    title = g_utf16_to_utf8 ((gunichar2*) szTitle, -1, NULL, NULL, NULL);
    history_item = gtk_ie_embed_history_item_new (uri, title);

    g_free (uri);
    g_free (title);
    CoTaskMemFree (szURL);
    CoTaskMemFree (szTitle);

    return history_item;
}

#endif

GList *
_ie_bridge_get_history (IEBridge *ie, GtkIEEmbedHistoryDirection direction)
{
    GList *history = NULL;
#ifdef HAVE_TLOGSTG_H
    IServiceProvider *service_provider = NULL;    
    ITravelLogStg *travel_log = NULL;
    IEnumTravelLogEntry *enum_entry = NULL;
    ITravelLogEntry *entry = NULL;
    TLENUMF direction_flags = (direction == GTK_IE_EMBED_HISTORY_FORWARD) ? TLEF_RELATIVE_FORE : TLEF_RELATIVE_BACK;

    travel_log = _get_travel_log (ie);
    if (!travel_log)
        return NULL;

    travel_log->EnumEntries (direction_flags, &enum_entry);
    if (!enum_entry) {
        travel_log->Release ();
        return NULL;
    }
    
    while (enum_entry->Next (1, &entry, NULL) != S_FALSE) {
        GtkIEEmbedHistoryItem *history_item;

        history_item = travel_log_entry_to_history_item (entry);
        history = g_list_append (history, history_item);

        entry->Release ();
        entry = NULL;
    }
    travel_log->Release ();
    enum_entry->Release ();

#endif
    return history;
}

#ifdef HAVE_TLOGSTG_H
static ITravelLogEntry *
create_travel_log_entry_from_history_item (ITravelLogStg *travel_log, GtkIEEmbedHistoryItem *item)
{
    ITravelLogEntry *entry = NULL;
    gunichar2 *utf16_uri, *utf16_title;

    utf16_uri = g_utf8_to_utf16 (gtk_ie_embed_history_item_get_uri (item), -1,
                                 NULL, NULL, NULL);
    utf16_title = g_utf8_to_utf16 (gtk_ie_embed_history_item_get_title (item), -1,
                                   NULL, NULL, NULL);

    travel_log->CreateEntry ((LPOLESTR) utf16_uri, (LPOLESTR) utf16_title,
                             NULL, FALSE, &entry);

    g_free (utf16_uri);
    g_free (utf16_title);

    return entry;
}

static void
remove_entries_from_travel_log (ITravelLogStg *travel_log, IEnumTravelLogEntry *enum_entries)
{
    ITravelLogEntry *entry = NULL;
    ITravelLogEntry *prev_entry = NULL;

    if (!enum_entries)
        return;

    while (enum_entries->Next (1, &entry, NULL) != S_FALSE) {
        if (prev_entry) {
            travel_log->RemoveEntry (prev_entry);
            prev_entry->Release ();
        }
        prev_entry = entry;
    }

    if (prev_entry) {
        travel_log->RemoveEntry (prev_entry);
        prev_entry->Release ();
    }
}
#endif

void
_ie_bridge_set_history (IEBridge *ie, GtkIEEmbedHistoryDirection direction, const GList *history)
{
#ifdef HAVE_TLOGSTG_H
    const GList *list;
    ITravelLogStg *travel_log = NULL;
    ITravelLogEntry *entry = NULL;
    IEnumTravelLogEntry *enum_entry = NULL;
    TLENUMF direction_flags = (direction == GTK_IE_EMBED_HISTORY_FORWARD) ? TLEF_RELATIVE_FORE : TLEF_RELATIVE_BACK;

    travel_log = _get_travel_log (ie);
    if (!travel_log)
        return;

    travel_log->EnumEntries (direction_flags, &enum_entry);
    remove_entries_from_travel_log (travel_log, enum_entry);
    enum_entry->Release ();

    for (list = g_list_last ((GList*) history); list; list = g_list_previous (list)) {
        GtkIEEmbedHistoryItem *item = GTK_IE_EMBED_HISTORY_ITEM (list->data);
        entry = create_travel_log_entry_from_history_item (travel_log, item);
        entry->Release ();
    }

    travel_log->Release ();
#endif
}

#ifdef HAVE_TLOGSTG_H
static void
cb_new_entry_loaded (GtkIEEmbed *embed, gpointer data)
{
    IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (data);
    ITravelLogStg *travel_log = NULL;
    ITravelLogEntry *entry = NULL;

    g_signal_handlers_disconnect_by_func (priv->widget,
                                          (gpointer)cb_new_entry_loaded,
                                          data);
    priv->is_setting_history = FALSE;

    travel_log = _get_travel_log (IE_BRIDGE (data));
    if (!travel_log)
        return;

    travel_log->RemoveEntry (priv->old_current_entry);
    entry->Release ();
}

static void
wait_for_loading_first_page (IEBridge *ie, const GList *history)
{
    GtkIEEmbedHistoryItem *item = GTK_IE_EMBED_HISTORY_ITEM (history->data);
    _ie_bridge_load_url (ie, gtk_ie_embed_history_item_get_uri(item));
    while (!_ie_bridge_is_ready (ie))
        g_main_context_iteration (NULL, FALSE);
}

#endif

void
_ie_bridge_set_whole_history (IEBridge *ie,
                              const GList *history,
                              guint current_position)
{
#ifdef HAVE_TLOGSTG_H
    const GList *list;
    ITravelLogStg *travel_log = NULL;
    ITravelLogEntry *entry = NULL;
    ITravelLogEntry *current_entry = NULL;
    ITravelLogEntry *old_current_entry = NULL;
    IEnumTravelLogEntry *enum_entry = NULL;
    guint count;
    gboolean not_ready = FALSE;

    if (!history)
        return;

    travel_log = _get_travel_log (ie);
    if (!travel_log)
        return;

    /* Load a page if the browser has not been loaded any page to avoid crash */
    if (!_ie_bridge_is_ready (ie)) {
        not_ready = TRUE;
        wait_for_loading_first_page (ie, history);
    } else {
        travel_log->EnumEntries (TLEF_ABSOLUTE, &enum_entry);
        if (enum_entry) {
            remove_entries_from_travel_log (travel_log, enum_entry);
            enum_entry->Release ();
        }
        /* current entry can not be removed so we remove it after create new entries */
        travel_log->GetRelativeEntry (0, &old_current_entry);
    }

    count = g_list_length ((GList *)history);
    if (not_ready)
        count--;

    for (list = g_list_last ((GList *)history); count > 0 && list; list = g_list_previous (list)) {
        GtkIEEmbedHistoryItem *item = GTK_IE_EMBED_HISTORY_ITEM (list->data);
        entry = create_travel_log_entry_from_history_item (travel_log, item);

        if (count + (not_ready ? 1 : 0 )== current_position)
            current_entry = entry;
        else
            entry->Release ();
        count--;
    }

    if (current_entry) {
        travel_log->TravelTo (current_entry);
        current_entry->Release ();
    }

    if (!not_ready) {
        IEBridgePriv *priv = IE_BRIDGE_GET_PRIVATE (ie);
        priv->old_current_entry = old_current_entry;
        g_signal_connect (priv->widget, "net-stop",
                          G_CALLBACK (cb_new_entry_loaded), ie);

        while (priv->is_setting_history) 
            g_main_context_iteration (NULL, FALSE);
    }
    travel_log->Release ();
#endif
}

guint
_ie_bridge_get_history_count (IEBridge *ie)
{
    DWORD count = 0;
#ifdef HAVE_TLOGSTG_H
    ITravelLogStg *travel_log = NULL;

    travel_log = _get_travel_log (ie);
    if (!travel_log)
        return 0;

    travel_log->GetCount (TLEF_ABSOLUTE, &count);
    travel_log->Release ();
#endif
    return (guint)count;
}

guint
_ie_bridge_get_current_position_in_history (IEBridge *ie)
{
    DWORD count = 0;
#ifdef HAVE_TLOGSTG_H
    ITravelLogStg *travel_log = NULL;

    travel_log = _get_travel_log (ie);
    if (!travel_log)
        return 0;

    travel_log->GetCount (TLEF_RELATIVE_BACK | 
                          TLEF_RELATIVE_INCLUDE_CURRENT, &count);
    travel_log->Release ();
#endif
    return (guint)count;
}

GtkIEEmbedHistoryItem *
_ie_bridge_get_history_item_at_relative_position (IEBridge *ie, gint offset)
{
    GtkIEEmbedHistoryItem *item = NULL;
#ifdef HAVE_TLOGSTG_H
    ITravelLogStg *travel_log = NULL;
    ITravelLogEntry *entry = NULL;

    travel_log = _get_travel_log (ie);
    if (!travel_log)
        return NULL;

    travel_log->GetRelativeEntry (offset, &entry);
    if (entry) {
        item = travel_log_entry_to_history_item (entry);
        entry->Release ();
    }
    travel_log->Release ();
#endif
    return item;
}

void
_ie_bridge_go_relative_position (IEBridge *ie, gint offset)
{
#ifdef HAVE_TLOGSTG_H
    ITravelLogStg *travel_log = NULL;
    ITravelLogEntry *entry = NULL;

    travel_log = _get_travel_log (ie);
    if (!travel_log)
        return;

    travel_log->GetRelativeEntry (offset, &entry);
    if (entry) {
        travel_log->TravelTo (entry);
        entry->Release ();
    }
    travel_log->Release ();
#endif
}


/*
vi:ts=4:nowrap:ai:expandtab:sw=4
*/
