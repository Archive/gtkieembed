/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 *  Copyright (C) 2007 Hiroyuki Ikezoe  <poincare@ikezoe.net>
 *
 *  This library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "ie-document-event-dispatcher.h"

#include <olectl.h>

IEDocumentEventDispatcher::IEDocumentEventDispatcher (IEBridge *bridge)
{
    mBridge = bridge;
    mRef = 0;
}

IEDocumentEventDispatcher::~IEDocumentEventDispatcher ()
{
    mBridge = 0;
}

HRESULT __stdcall
IEDocumentEventDispatcher::QueryInterface (REFIID riid, void **ppvObject)
{
    if (ppvObject == 0)
        return E_NOINTERFACE;

    if (IsEqualIID (riid, IID_IUnknown) ||
        IsEqualIID (riid, IID_IDispatch) ||
        IsEqualIID (riid, DIID_HTMLDocumentEvents)) {
        *ppvObject = (IUnknown *) this;
        return S_OK;
    }

    *ppvObject = 0;

    return E_NOINTERFACE;
}

ULONG __stdcall
IEDocumentEventDispatcher::AddRef ()
{
    return ++mRef;
}

ULONG __stdcall
IEDocumentEventDispatcher::Release ()
{
    return --mRef;
}

HRESULT __stdcall
IEDocumentEventDispatcher::GetTypeInfoCount (UINT*)
{
    return E_NOTIMPL;
}

HRESULT __stdcall
IEDocumentEventDispatcher::GetTypeInfo (UINT,LCID,LPTYPEINFO*)
{
    return E_NOTIMPL;
}

HRESULT __stdcall
IEDocumentEventDispatcher::GetIDsOfNames (REFIID,LPOLESTR*,UINT,LCID,DISPID*)
{
    return E_NOTIMPL;
}

HRESULT __stdcall
IEDocumentEventDispatcher::Invoke (DISPID dispIdMember,
                                   REFIID riid,
                                   LCID lcid,
                                   WORD wFlags,
                                   DISPPARAMS* pDispParams, 
                                   VARIANT* pVarResult,
                                   EXCEPINFO* pExcepInfo,
                                   UINT* puArgErr)
{
    VARIANT_BOOL bEventRet;

    switch (dispIdMember)
    {
     case DISPID_MOUSEDOWN:
        MouseDown (&bEventRet);
        if (bEventRet == VARIANT_TRUE) {
            pVarResult->vt = VT_BOOL;
            pVarResult->boolVal = bEventRet;
        }
        break;
     case DISPID_MOUSEMOVE:
        MouseMove (&bEventRet);
        if (bEventRet == VARIANT_TRUE) {
            pVarResult->vt = VT_BOOL;
            pVarResult->boolVal = bEventRet;
        }
        break;
     case DISPID_MOUSEUP:
        MouseUp (&bEventRet);
        if (bEventRet == VARIANT_TRUE) {
            pVarResult->vt = VT_BOOL;
            pVarResult->boolVal = bEventRet;
        }
        break;
     case DISPID_CLICK:
        Click (&bEventRet);
        if (bEventRet == VARIANT_TRUE) {
            pVarResult->vt = VT_BOOL;
            pVarResult->boolVal = bEventRet;
        }
        break;
     case 1023: /* DISPID_ONCONTEXTMENU */
        if (!_ie_bridge_get_use_context_menu (mBridge)) {
            pVarResult->vt = VT_BOOL;
            pVarResult->boolVal = VARIANT_FALSE;
        }
        break;
     case 1037: /* DISPID_ONSELECTIONCHANGE */
        _ie_bridge_selection_changed (mBridge);
        break;
     case 1027: /* DISPID_ONBEFOREEDITFOCUS */
     case 1034: /* DISPID_ONBEDOREDEACTIVATE */
     case 1047: /* DISPID_ONBEFOREACTIVATE */
        /* cancel if widget is not mapped */
        if (!_ie_bridge_is_mapped (mBridge)) {
            pVarResult->vt = VT_BOOL;
            pVarResult->boolVal = VARIANT_FALSE;
        }
        break;
#if 0
     case 1044: /* DISPID_ONACTIVATE */
     case 1045: /* DISPID_ONDEACTIVATE */
     case 1048: /* DISPID_ONFOCUSIN */
     case 1049: /* DISPID_ONFOCUSOUT */
        if (!_ie_bridge_is_mapped (mBridge)) {
            pVarResult->vt = VT_BOOL;
            pVarResult->boolVal = VARIANT_FALSE;
        }
        break;
#endif
     default:
#if 0
        if (dispIdMember > 1000) {
            gchar *t;
            t = g_strdup_printf ("%d", dispIdMember);
            gunichar2 *title = g_utf8_to_utf16 (t, -1, NULL, NULL, NULL);
            _ie_bridge_title_changed (mBridge, title);
            g_free (title);
            g_free (t);
        }
#endif
        break;
    }

    return S_OK;
}

void 
IEDocumentEventDispatcher::MouseDown (VARIANT_BOOL *bProcessEvent)
{
    *bProcessEvent = _ie_bridge_mouse_down (mBridge) ? VARIANT_TRUE : VARIANT_FALSE;
}

void 
IEDocumentEventDispatcher::MouseMove (VARIANT_BOOL *bProcessEvent)
{
    *bProcessEvent = _ie_bridge_mouse_move (mBridge) ? VARIANT_TRUE : VARIANT_FALSE;
}

void 
IEDocumentEventDispatcher::MouseUp (VARIANT_BOOL *bProcessEvent)
{
    *bProcessEvent = _ie_bridge_mouse_up (mBridge) ? VARIANT_TRUE : VARIANT_FALSE;
}

void 
IEDocumentEventDispatcher::Click (VARIANT_BOOL *bProcessEvent)
{
    *bProcessEvent = _ie_bridge_mouse_click (mBridge) ? VARIANT_TRUE : VARIANT_FALSE;
}

/*
vi:ts=4:nowrap:ai:expandtab:sw=4
*/
