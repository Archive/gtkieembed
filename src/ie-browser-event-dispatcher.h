/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 *  Copyright (C) 2007 Hiroyuki Ikezoe  <poincare@ikezoe.net>
 *
 *  This library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __IE_BROWSER_EVENT_DISPATCHER__
#define __IE_BROWSER_EVENT_DISPATCHER__

#include <windows.h>

#include "ie-bridge.h"

class IEBrowserEventDispatcher : public IDispatch
{
 public:
     IEBrowserEventDispatcher (IEBridge *);
     ~IEBrowserEventDispatcher ();

     STDMETHOD(QueryInterface) (REFIID,PVOID*);
     STDMETHOD_(ULONG, AddRef) ();
     STDMETHOD_(ULONG, Release) ();
     STDMETHOD(GetTypeInfoCount) (UINT*);
     STDMETHOD(GetTypeInfo) (UINT,LCID,LPTYPEINFO*);
     STDMETHOD(GetIDsOfNames) (REFIID,LPOLESTR*,UINT,LCID,DISPID*);
     STDMETHOD(Invoke) (DISPID,REFIID,LCID,WORD,DISPPARAMS*,VARIANT*,EXCEPINFO*,UINT*);

     STDMETHOD_(void, StatusTextChange) (BSTR);
     STDMETHOD_(void, ProgressChange) (long,long);
     STDMETHOD_(void, CommandStateChange) (long,VARIANT_BOOL);
     STDMETHOD_(void, DownloadBegin) ();
     STDMETHOD_(void, DownloadComplete) ();
     STDMETHOD_(void, TitleChange) (BSTR);
     STDMETHOD_(void, PropertyChange) (BSTR);
     STDMETHOD_(void, BeforeNavigate2) (IDispatch*,VARIANT*,VARIANT*,VARIANT*,VARIANT*,VARIANT*,VARIANT_BOOL*);
     STDMETHOD_(void, NewWindow2) (IDispatch**,VARIANT_BOOL*);
     STDMETHOD_(void, NavigateComplete2) (IDispatch*,VARIANT*);
     STDMETHOD_(void, DocumentComplete) (IDispatch*,VARIANT*);
     STDMETHOD_(void, WindowClosing) (VARIANT_BOOL,VARIANT_BOOL*);
     STDMETHOD_(void, ClientToHostWindow) (long*,long*);
     STDMETHOD_(void, SetSecureLockIcon) (long);
     STDMETHOD_(void, FileDownload) (VARIANT_BOOL*);
     STDMETHOD_(void, PrivacyImpactedStateChange) (VARIANT_BOOL);
     STDMETHOD_(void, OnVisible) (VARIANT_BOOL);
 private:
     IEBridge *mBridge;
     ULONG mRef;
     BOOL mReady;
};

#endif /* __IE_BROWSER_EVENT_DISPATCHER__ */
/*
vi:ts=4:nowrap:ai:expandtab:sw=4
*/
