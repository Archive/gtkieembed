/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 *  Copyright (C) 2007 Hiroyuki Ikezoe  <poincare@ikezoe.net>
 *
 *  This library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __IE_DOCUMENT_EVENT_DISPATCHER__
#define __IE_DOCUMENT_EVENT_DISPATCHER__

#include <windows.h>
#include "ie-bridge.h"

#ifdef _WINDOWS
#include <mshtml.h>
#else
const IID DIID_HTMLDocumentEvents = {0x3050f260, 0x98b5, 0x11cf, 0xbb,0x82, 0x00,0xaa,0x00,0xbd,0xce,0x0b};
#endif /* _WINDOWS */

class IEDocumentEventDispatcher : public IDispatch
{
 public:
     IEDocumentEventDispatcher (IEBridge *bridge);
     ~IEDocumentEventDispatcher ();

     STDMETHOD(QueryInterface) (REFIID,PVOID*);
     STDMETHOD_(ULONG, AddRef) ();
     STDMETHOD_(ULONG, Release) ();
     STDMETHOD(GetTypeInfoCount) (UINT*);
     STDMETHOD(GetTypeInfo) (UINT,LCID,LPTYPEINFO*);
     STDMETHOD(GetIDsOfNames) (REFIID,LPOLESTR*,UINT,LCID,DISPID*);
     STDMETHOD(Invoke) (DISPID,REFIID,LCID,WORD,DISPPARAMS*,VARIANT*,EXCEPINFO*,UINT*);

     void MouseDown (VARIANT_BOOL*);
     void MouseMove (VARIANT_BOOL*);
     void MouseUp (VARIANT_BOOL*);
     void Click (VARIANT_BOOL*);
 private:
     IEBridge *mBridge;
     ULONG mRef;
};

#endif /* __IE_DOCUMENT_EVENT_DISPATCHER__ */
/*
vi:ts=4:nowrap:ai:expandtab:sw=4
*/
