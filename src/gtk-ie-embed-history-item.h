/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 *  Copyright (C) 2009 Hiroyuki Ikezoe  <poincare@ikezoe.net>
 *
 *  This library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __GTK_IE_EMBED_HISTORY_ITEM_H__
#define __GTK_IE_EMBED_HISTORY_ITEM_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define GTK_TYPE_IE_EMBED_HISTORY_ITEM            (gtk_ie_embed_history_item_get_type ())
#define GTK_IE_EMBED_HISTORY_ITEM(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTK_TYPE_IE_EMBED_HISTORY_ITEM, GtkIEEmbedHistoryItem))
#define GTK_IE_EMBED_HISTORY_ITEM_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GTK_TYPE_IE_EMBED_HISTORY_ITEM, GtkIEEmbedHistoryItemClass))
#define GTK_IS_IE_EMBED_HISTORY_ITEM(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTK_TYPE_IE_EMBED_HISTORY_ITEM))
#define GTK_IS_IE_EMBED_HISTORY_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GTK_TYPE_IE_EMBED_HISTORY_ITEM))
#define GTK_IE_EMBED_HISTORY_ITEM_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj), GTK_TYPE_IE_EMBED_HISTORY_ITEM, GtkIEEmbedHistoryItemClass))

typedef enum {
    GTK_IE_EMBED_HISTORY_FORWARD,
    GTK_IE_EMBED_HISTORY_BACKWARD
} GtkIEEmbedHistoryDirection;

typedef struct _GtkIEEmbedHistoryItem GtkIEEmbedHistoryItem;
typedef struct _GtkIEEmbedHistoryItemClass GtkIEEmbedHistoryItemClass;

struct _GtkIEEmbedHistoryItem
{
    GObject parent;
};

struct _GtkIEEmbedHistoryItemClass
{
    GObjectClass parent_class;
};

GType        gtk_ie_embed_history_item_get_type        (void) G_GNUC_CONST;

GtkIEEmbedHistoryItem
            *gtk_ie_embed_history_item_new             (const gchar *uri,
                                                        const gchar *title);

const gchar *gtk_ie_embed_history_item_get_uri         (GtkIEEmbedHistoryItem *item);
void         gtk_ie_embed_history_item_set_uri         (GtkIEEmbedHistoryItem *item,
                                                        const gchar *uri);
const gchar *gtk_ie_embed_history_item_get_title       (GtkIEEmbedHistoryItem *item);
void         gtk_ie_embed_history_item_set_title       (GtkIEEmbedHistoryItem *item,
                                                        const gchar *title);

G_END_DECLS

#endif /* __GTK_IE_EMBED_HISTORY_ITEM_H__ */

/*
vi:ts=4:nowrap:ai:expandtab:sw=4
*/

