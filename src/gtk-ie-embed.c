/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 *  Copyright (C) 2007 Hiroyuki Ikezoe  <poincare@ikezoe.net>
 *
 *  This library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif /* HAVE_CONFIG_H */

#include "gtk-ie-embed.h"

#include "gtk-ie-embed-private.h"

#include "ie-bridge.h"
#include "gtk-ie-embed-marshalers.h"

enum {
    LOCATION_SIGNAL,
    TITLE_SIGNAL,
    PROGRESS_SIGNAL,
    NET_START_SIGNAL,
    NET_STOP_SIGNAL,
    STATUS_TEXT_SIGNAL,
    NEW_WINDOW_SIGNAL,
    CLOSE_WINDOW_SIGNAL,
    DOM_MOUSE_DOWN_SIGNAL,
    DOM_MOUSE_MOVE_SIGNAL,
    DOM_MOUSE_UP_SIGNAL,
    DOM_MOUSE_CLICK_SIGNAL,
    SELECTION_CHANGED_SIGNAL,
    FAVICON_SIGNAL,
    LAST_SIGNAL
};

static gint gtk_ie_embed_signals[LAST_SIGNAL] = {0};

enum {
    PROP_0,
    PROP_USE_CONTEXT_MENU
};

typedef struct _GtkIEEmbedPriv GtkIEEmbedPriv;
struct _GtkIEEmbedPriv
{
    IEBridge *bridge;
    gboolean use_context_menu;
};

#define GTK_IE_EMBED_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), GTK_TYPE_IE_EMBED, GtkIEEmbedPriv))

G_DEFINE_TYPE (GtkIEEmbed, gtk_ie_embed, GTK_TYPE_WIDGET)

static void   dispose              (GObject           *object);
static void   set_property         (GObject           *object,
                                    guint              prop_id,
                                    const GValue      *value,
                                    GParamSpec        *pspec);
static void   get_property         (GObject           *object,
                                    guint              prop_id,
                                    GValue            *value,
                                    GParamSpec        *pspec);
static void   size_allocate        (GtkWidget         *widget,
                                    GtkAllocation     *allocation);
static void   realize              (GtkWidget         *widget);
static void   unrealize            (GtkWidget         *widget);
static void   map                  (GtkWidget         *widget);
static void   unmap                (GtkWidget         *widget);

static void
gtk_ie_embed_class_init (GtkIEEmbedClass *klass)
{
    GObjectClass   *gobject_class = G_OBJECT_CLASS (klass);
    GtkWidgetClass *widget_class  = GTK_WIDGET_CLASS (klass);

    gobject_class->dispose             = dispose;
    gobject_class->set_property        = set_property;
    gobject_class->get_property        = get_property;

    widget_class->size_allocate        = size_allocate;
    widget_class->realize              = realize;
    widget_class->unrealize            = unrealize;
    widget_class->map                  = map;
    widget_class->unmap                = unmap;

    g_object_class_install_property (
        gobject_class,
        PROP_USE_CONTEXT_MENU,
        g_param_spec_boolean (
            "use-context-menu",
            "Use context menu",
            "If set, default context menu is used",
            TRUE,
            (GParamFlags) (G_PARAM_READWRITE)));

    gtk_ie_embed_signals[LOCATION_SIGNAL]
        = g_signal_new ("location",
                GTK_TYPE_IE_EMBED,
                G_SIGNAL_RUN_FIRST,
                G_STRUCT_OFFSET(GtkIEEmbedClass, location),
                NULL, NULL,
                g_cclosure_marshal_VOID__STRING,
                G_TYPE_NONE, 1,
                G_TYPE_STRING);

    gtk_ie_embed_signals[TITLE_SIGNAL]
        = g_signal_new ("title",
                GTK_TYPE_IE_EMBED,
                G_SIGNAL_RUN_FIRST,
                G_STRUCT_OFFSET(GtkIEEmbedClass, title),
                NULL, NULL,
                g_cclosure_marshal_VOID__STRING,
                G_TYPE_NONE, 1,
                G_TYPE_STRING);

    gtk_ie_embed_signals[PROGRESS_SIGNAL]
        = g_signal_new ("progress",
                GTK_TYPE_IE_EMBED,
                G_SIGNAL_RUN_FIRST,
                G_STRUCT_OFFSET(GtkIEEmbedClass, progress),
                NULL, NULL,
                _gtk_ie_embed_marshal_VOID__LONG_LONG,
                G_TYPE_NONE, 2,
                G_TYPE_LONG, G_TYPE_LONG);

    gtk_ie_embed_signals[NET_START_SIGNAL]
        = g_signal_new ("net-start",
                GTK_TYPE_IE_EMBED,
                G_SIGNAL_RUN_FIRST,
                G_STRUCT_OFFSET(GtkIEEmbedClass, net_start),
                NULL, NULL,
                g_cclosure_marshal_VOID__VOID,
                G_TYPE_NONE, 0);

    gtk_ie_embed_signals[NET_STOP_SIGNAL]
        = g_signal_new ("net-stop",
                GTK_TYPE_IE_EMBED,
                G_SIGNAL_RUN_FIRST,
                G_STRUCT_OFFSET(GtkIEEmbedClass, net_stop),
                NULL, NULL,
                g_cclosure_marshal_VOID__VOID,
                G_TYPE_NONE, 0);

    gtk_ie_embed_signals[STATUS_TEXT_SIGNAL]
        = g_signal_new ("status-text",
                GTK_TYPE_IE_EMBED,
                G_SIGNAL_RUN_FIRST,
                G_STRUCT_OFFSET(GtkIEEmbedClass, status_text),
                NULL, NULL,
                g_cclosure_marshal_VOID__STRING,
                G_TYPE_NONE, 1,
                G_TYPE_STRING);

    gtk_ie_embed_signals[NEW_WINDOW_SIGNAL]
        = g_signal_new ("new-window",
                GTK_TYPE_IE_EMBED,
                G_SIGNAL_RUN_LAST,
                G_STRUCT_OFFSET(GtkIEEmbedClass, new_window),
                NULL, NULL,
                _gtk_ie_embed_marshal_BOOLEAN__POINTER,
                G_TYPE_BOOLEAN, 1,
                G_TYPE_POINTER);

    gtk_ie_embed_signals[CLOSE_WINDOW_SIGNAL]
        = g_signal_new ("close-window",
                GTK_TYPE_IE_EMBED,
                G_SIGNAL_RUN_LAST,
                G_STRUCT_OFFSET(GtkIEEmbedClass, close_window),
                NULL, NULL,
                _gtk_ie_embed_marshal_BOOLEAN__VOID,
                G_TYPE_BOOLEAN, 0);

    gtk_ie_embed_signals[DOM_MOUSE_DOWN_SIGNAL]
        = g_signal_new ("dom-mouse-down",
                GTK_TYPE_IE_EMBED,
                G_SIGNAL_RUN_LAST,
                G_STRUCT_OFFSET(GtkIEEmbedClass, dom_mouse_down),
                NULL, NULL,
                _gtk_ie_embed_marshal_BOOLEAN__POINTER,
                G_TYPE_BOOLEAN, 1,
                G_TYPE_POINTER);

    gtk_ie_embed_signals[DOM_MOUSE_MOVE_SIGNAL]
        = g_signal_new ("dom-mouse-move",
                GTK_TYPE_IE_EMBED,
                G_SIGNAL_RUN_LAST,
                G_STRUCT_OFFSET(GtkIEEmbedClass, dom_mouse_move),
                NULL, NULL,
                _gtk_ie_embed_marshal_BOOLEAN__POINTER,
                G_TYPE_BOOLEAN, 1,
                G_TYPE_POINTER);

    gtk_ie_embed_signals[DOM_MOUSE_UP_SIGNAL]
        = g_signal_new ("dom-mouse-up",
                GTK_TYPE_IE_EMBED,
                G_SIGNAL_RUN_LAST,
                G_STRUCT_OFFSET(GtkIEEmbedClass, dom_mouse_up),
                NULL, NULL,
                _gtk_ie_embed_marshal_BOOLEAN__POINTER,
                G_TYPE_BOOLEAN, 1,
                G_TYPE_POINTER);

    gtk_ie_embed_signals[DOM_MOUSE_CLICK_SIGNAL]
        = g_signal_new ("dom-mouse-click",
                GTK_TYPE_IE_EMBED,
                G_SIGNAL_RUN_LAST,
                G_STRUCT_OFFSET(GtkIEEmbedClass, dom_mouse_click),
                NULL, NULL,
                _gtk_ie_embed_marshal_BOOLEAN__POINTER,
                G_TYPE_BOOLEAN, 1,
                G_TYPE_POINTER);

    gtk_ie_embed_signals[SELECTION_CHANGED_SIGNAL]
        = g_signal_new ("selection-changed",
                GTK_TYPE_IE_EMBED,
                G_SIGNAL_RUN_FIRST,
                G_STRUCT_OFFSET(GtkIEEmbedClass, selection_changed),
                NULL, NULL,
                g_cclosure_marshal_VOID__VOID,
                G_TYPE_NONE, 0);

    gtk_ie_embed_signals[FAVICON_SIGNAL]
        = g_signal_new ("favicon",
                GTK_TYPE_IE_EMBED,
                G_SIGNAL_RUN_FIRST,
                G_STRUCT_OFFSET(GtkIEEmbedClass, favicon),
                NULL, NULL,
                g_cclosure_marshal_VOID__OBJECT,
                G_TYPE_NONE, 1,
                GDK_TYPE_PIXBUF);

    g_type_class_add_private (gobject_class, sizeof (GtkIEEmbedPriv));
}

static void
gtk_ie_embed_init (GtkIEEmbed *ie_embed)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie_embed);

    priv->bridge = NULL;
    priv->use_context_menu = TRUE;
}

static void
dispose (GObject *object)
{
    if (G_OBJECT_CLASS (gtk_ie_embed_parent_class)->dispose)
        G_OBJECT_CLASS (gtk_ie_embed_parent_class)->dispose (object);
}

static void
set_property (GObject *object,
              guint prop_id,
              const GValue *value,
              GParamSpec *pspec)
{
    switch (prop_id) {
    case PROP_USE_CONTEXT_MENU:
          gtk_ie_embed_set_use_context_menu (GTK_IE_EMBED (object), g_value_get_boolean (value));
          break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
get_property (GObject *object,
              guint prop_id,
              GValue *value,
              GParamSpec *pspec)
{
    switch (prop_id) {
    case PROP_USE_CONTEXT_MENU:
          g_value_set_boolean (value, gtk_ie_embed_get_use_context_menu (GTK_IE_EMBED (object)));
          break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

GtkWidget *
gtk_ie_embed_new (void)
{
    return GTK_WIDGET (g_object_new (GTK_TYPE_IE_EMBED, NULL));
}

static void
realize (GtkWidget *widget)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (widget);

    GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);

    priv->bridge = _ie_bridge_new (widget);
}

static void
unrealize (GtkWidget *widget)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (widget);

    if (priv->bridge) {
        g_object_unref (priv->bridge);
        priv->bridge = NULL;
    }

    if (GTK_WIDGET_CLASS (gtk_ie_embed_parent_class)->unrealize)
        GTK_WIDGET_CLASS (gtk_ie_embed_parent_class)->unrealize (widget);
}

static void
map (GtkWidget *widget)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (widget);

    if (GTK_WIDGET_CLASS (gtk_ie_embed_parent_class)->map)
        GTK_WIDGET_CLASS (gtk_ie_embed_parent_class)->map (widget);

    if (priv->bridge)
        _ie_bridge_set_visible (priv->bridge, TRUE);

}

static void
unmap (GtkWidget *widget)
{ 
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (widget);

    if (GTK_WIDGET_CLASS (gtk_ie_embed_parent_class)->unmap)
        GTK_WIDGET_CLASS (gtk_ie_embed_parent_class)->unmap (widget);

    if (priv->bridge) {
        _ie_bridge_set_visible (priv->bridge, FALSE);
    }
}

static void
size_allocate (GtkWidget *widget, GtkAllocation *allocation)
{
    g_return_if_fail(allocation);

    widget->allocation = *allocation;
    if (GTK_WIDGET_REALIZED (widget)) {
        gdk_window_move_resize (widget->window,
                                allocation->x,
                                allocation->y,
                                allocation->width,
                                allocation->height);
    }
}

void
gtk_ie_embed_load_url (GtkIEEmbed *ie, const gchar *url)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    if (!priv->bridge)
        return;

    _ie_bridge_load_url (priv->bridge, url);
}

void
gtk_ie_embed_load_html_from_string (GtkIEEmbed *ie, const gchar *string)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    if (!priv->bridge)
        return;

    _ie_bridge_load_html_from_string (priv->bridge, string);
}

void
gtk_ie_embed_reload (GtkIEEmbed *ie, GtkIEEmbedReloadFlag flag)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);
    RefreshConstants ref_flag;

    if (!priv->bridge)
        return;

    switch (flag) {
     case GTK_IE_EMBED_RELOAD_NORMAL:
        ref_flag = REFRESH_NORMAL;
        break;
     case GTK_IE_EMBED_RELOAD_IFEXPIRED:
        ref_flag = REFRESH_IFEXPIRED;
        break;
     case GTK_IE_EMBED_RELOAD_COMPLETELY:
        ref_flag = REFRESH_COMPLETELY;
        break;
     default:
        ref_flag = REFRESH_NORMAL;
        break;
    }
    _ie_bridge_reload (priv->bridge, ref_flag);
}

void
gtk_ie_embed_stop (GtkIEEmbed *ie)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    if (!priv->bridge)
        return;

    _ie_bridge_stop (priv->bridge);
}

gboolean
gtk_ie_embed_is_loading (GtkIEEmbed *ie)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    if (!priv->bridge)
        return FALSE;

    return _ie_bridge_is_loading (priv->bridge);
}

void
gtk_ie_embed_go_back (GtkIEEmbed *ie)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    if (!priv->bridge)
        return;

    _ie_bridge_go_back (priv->bridge);
}

void
gtk_ie_embed_go_forward (GtkIEEmbed *ie)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    if (!priv->bridge)
        return;

    _ie_bridge_go_forward (priv->bridge);
}

void
gtk_ie_embed_go_relative_position (GtkIEEmbed *ie, gint offset)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    if (!priv->bridge)
        return;

    _ie_bridge_go_relative_position (priv->bridge, offset);
}

gchar *
gtk_ie_embed_get_location (GtkIEEmbed *ie)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    if (!priv->bridge)
        return NULL;

    return _ie_bridge_get_location (priv->bridge);
}

gchar *
gtk_ie_embed_get_title (GtkIEEmbed *ie)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    if (!priv->bridge)
        return NULL;

    return _ie_bridge_get_title (priv->bridge);
}

gboolean
gtk_ie_embed_can_go_forward (GtkIEEmbed *ie)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    if (!priv->bridge)
        return FALSE;

    return _ie_bridge_can_go_forward (priv->bridge);
}

gboolean
gtk_ie_embed_can_go_back (GtkIEEmbed *ie)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    if (!priv->bridge)
        return FALSE;

    return _ie_bridge_can_go_back (priv->bridge);
}

void
gtk_ie_embed_cut_clipboard (GtkIEEmbed *ie)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    if (!priv->bridge)
        return;

    _ie_bridge_cut_clipboard (priv->bridge);
}

void
gtk_ie_embed_copy_clipboard (GtkIEEmbed *ie)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    if (!priv->bridge)
        return;

    _ie_bridge_copy_clipboard (priv->bridge);
}

void
gtk_ie_embed_paste_clipboard (GtkIEEmbed *ie)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    if (!priv->bridge)
        return;

    _ie_bridge_paste_clipboard (priv->bridge);
}

void
gtk_ie_embed_select_all (GtkIEEmbed *ie)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    if (!priv->bridge)
        return;

    _ie_bridge_select_all (priv->bridge);
}

gboolean
gtk_ie_embed_can_cut_clipboard (GtkIEEmbed *ie)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    if (!priv->bridge)
        return FALSE;

    return _ie_bridge_can_cut_clipboard (priv->bridge);
}

gboolean
gtk_ie_embed_can_copy_clipboard (GtkIEEmbed *ie)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    if (!priv->bridge)
        return FALSE;

    return _ie_bridge_can_copy_clipboard (priv->bridge);
}

gboolean
gtk_ie_embed_can_paste_clipboard (GtkIEEmbed *ie)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    if (!priv->bridge)
        return FALSE;

    return _ie_bridge_can_paste_clipboard (priv->bridge);
}

void
gtk_ie_embed_set_font_size (GtkIEEmbed *ie, GtkIEEmbedFontSize size)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    if (!priv->bridge)
        return;

    _ie_bridge_set_font_size (priv->bridge, size);
}

GtkIEEmbedFontSize
gtk_ie_embed_get_font_size (GtkIEEmbed *ie)
{
    GtkIEEmbedFontSize size;
    gint usize;
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    if (!priv->bridge)
        return GTK_IE_EMBED_FONT_MEDIUM;

    usize = _ie_bridge_get_font_size (priv->bridge);

    switch (usize) {
     case 0:
        size = GTK_IE_EMBED_FONT_SMALLEST;
        break;
     case 1:
        size = GTK_IE_EMBED_FONT_SMALL;
        break;
     case 2:
        size = GTK_IE_EMBED_FONT_MEDIUM;
        break;
     case 3:
        size = GTK_IE_EMBED_FONT_LARGE;
        break;
     case 4:
        size = GTK_IE_EMBED_FONT_LARGEST;
        break;
     default:
        size = GTK_IE_EMBED_FONT_MEDIUM;
        break;
    }
    return size;
}

void
gtk_ie_embed_print (GtkIEEmbed *ie)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    if (!priv->bridge)
        return;

    _ie_bridge_print (priv->bridge);
}

void
gtk_ie_embed_page_setup (GtkIEEmbed *ie)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    if (!priv->bridge)
        return;

    _ie_bridge_page_setup (priv->bridge);
}

void
gtk_ie_embed_print_preview (GtkIEEmbed *ie)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    if (!priv->bridge)
        return;

    _ie_bridge_print_preview (priv->bridge);
}

gchar *
gtk_ie_embed_get_charset (GtkIEEmbed *ie)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    if (!priv->bridge)
        return NULL;

    return _ie_bridge_get_charset (priv->bridge);
}

void
gtk_ie_embed_set_charset (GtkIEEmbed *ie, const gchar *charset)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    if (!priv->bridge)
        return;

    return _ie_bridge_set_charset (priv->bridge, charset);
}

void
gtk_ie_embed_save_as (GtkIEEmbed *ie, const gchar *filename)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    if (!priv->bridge)
        return;

    return _ie_bridge_save (priv->bridge, filename);
}

gchar *
gtk_ie_embed_get_last_modified (GtkIEEmbed *ie)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    if (!priv->bridge)
        return NULL;

    return _ie_bridge_get_last_modified (priv->bridge);
}

gchar *
gtk_ie_embed_get_selected_text (GtkIEEmbed *ie)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    if (!priv->bridge)
        return NULL;

    return _ie_bridge_get_selected_text (priv->bridge);
}

gboolean
gtk_ie_embed_find_string (GtkIEEmbed *ie,
                          const gchar *string,
                          gboolean forward,
                          gboolean wrap)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    if (!priv->bridge)
        return FALSE;

    return _ie_bridge_find_string (priv->bridge,
                                   string,
                                   forward,
                                   wrap);
}

void
gtk_ie_embed_set_use_context_menu (GtkIEEmbed *ie, gboolean use)
{
    GTK_IE_EMBED_GET_PRIVATE (ie)->use_context_menu = use;
}

gboolean
gtk_ie_embed_get_use_context_menu (GtkIEEmbed *ie)
{
    return GTK_IE_EMBED_GET_PRIVATE (ie)->use_context_menu;
}

GList *
gtk_ie_embed_get_backward_history (GtkIEEmbed *ie)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    return priv->bridge ? _ie_bridge_get_history (priv->bridge, GTK_IE_EMBED_HISTORY_BACKWARD) : NULL;
}

GList *
gtk_ie_embed_get_forward_history (GtkIEEmbed *ie)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    return priv->bridge ? _ie_bridge_get_history (priv->bridge, GTK_IE_EMBED_HISTORY_FORWARD) : NULL;
}

void
gtk_ie_embed_set_backward_history (GtkIEEmbed *ie, const GList *history)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    return priv->bridge ? _ie_bridge_set_history (priv->bridge, GTK_IE_EMBED_HISTORY_BACKWARD, history) : NULL;
}

void
gtk_ie_embed_set_forward_history (GtkIEEmbed *ie, const GList *history)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    return priv->bridge ? _ie_bridge_set_history (priv->bridge, GTK_IE_EMBED_HISTORY_FORWARD, history) : NULL;
}

void
gtk_ie_embed_set_whole_history (GtkIEEmbed *ie,
                                const GList *history,
                                guint current_position)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    return priv->bridge ? _ie_bridge_set_whole_history (priv->bridge, history, current_position) : NULL;
}

guint
gtk_ie_embed_get_history_count (GtkIEEmbed *ie)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    return priv->bridge ? _ie_bridge_get_history_count (priv->bridge) : 0;
}

guint
gtk_ie_embed_get_current_position_in_history (GtkIEEmbed *ie)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    return priv->bridge ? _ie_bridge_get_current_position_in_history (priv->bridge) : 0;
}

GtkIEEmbedHistoryItem *
gtk_ie_embed_get_history_item_at_relative_position (GtkIEEmbed *ie, gint position)
{
    GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (ie);

    return priv->bridge ? _ie_bridge_get_history_item_at_relative_position (priv->bridge, position) : NULL;
}

const gchar *
gtk_ie_embed_dom_event_target_get_name (GtkIEEmbedDOMEventTarget *target)
{
    return target ? target->name : NULL;
}

const gchar *
gtk_ie_embed_dom_event_target_get_attribute_value (GtkIEEmbedDOMEventTarget *target,
                                                   const gchar *attribute_name)
{
    GList *list;

    if (!target)
        return NULL;

    for (list = target->attribute_list; list; list = g_list_next (list)) {
        GtkIEEmbedDOMEventTargetAttribute *attr = list->data;

        if (attr->name && !g_ascii_strcasecmp (attr->name, attribute_name))
            return attr->value;
    }

    return NULL;
}

const GList *
gtk_ie_embed_dom_event_target_get_attributes (GtkIEEmbedDOMEventTarget *target)
{
    return target ? target->attribute_list : NULL;
}

void 
gtk_ie_embed_dom_event_target_free (GtkIEEmbedDOMEventTarget *target)
{
    GList *list;

    if (!target)
        return;

    g_free (target->name);

    for (list = target->attribute_list; list; list = g_list_next (list)) {
        GtkIEEmbedDOMEventTargetAttribute *attr = list->data;

        if (attr->name)
            g_free (attr->name);
        if (attr->value)
            g_free (attr->value);
        g_slice_free (GtkIEEmbedDOMEventTargetAttribute, attr);
    }
    g_list_free (list);

    g_slice_free (GtkIEEmbedDOMEventTarget, target);
}

gboolean
_gtk_ie_embed_new_window (GtkIEEmbed *ie, IEBridge **newbridge)
{
    gboolean cancel = FALSE;
    GtkIEEmbed *newie = NULL;

    g_signal_emit_by_name (ie, "new-window", &newie, &cancel);

    if (newie) {
        GtkIEEmbedPriv *priv = GTK_IE_EMBED_GET_PRIVATE (newie);
        *newbridge = priv->bridge;
    }
    return cancel;
}

/*
vi:ts=4:nowrap:ai:expandtab:sw=4
*/
