/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 *  Copyright (C) 2009 Hiroyuki Ikezoe  <poincare@ikezoe.net>
 *
 *  This library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif /* HAVE_CONFIG_H */

#include "gtk-ie-embed-history-item.h"

enum {
    PROP_0,
    PROP_URI,
    PROP_TITLE
};

typedef struct _GtkIEEmbedHistoryItemPriv GtkIEEmbedHistoryItemPriv;
struct _GtkIEEmbedHistoryItemPriv
{
    gchar *uri;
    gchar *title;
};

#define GTK_IE_EMBED_HISTORY_ITEM_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), GTK_TYPE_IE_EMBED_HISTORY_ITEM, GtkIEEmbedHistoryItemPriv))

G_DEFINE_TYPE (GtkIEEmbedHistoryItem, gtk_ie_embed_history_item, G_TYPE_OBJECT)

static void   dispose              (GObject           *object);
static void   set_property         (GObject           *object,
                                    guint              prop_id,
                                    const GValue      *value,
                                    GParamSpec        *pspec);
static void   get_property         (GObject           *object,
                                    guint              prop_id,
                                    GValue            *value,
                                    GParamSpec        *pspec);
static void
gtk_ie_embed_history_item_class_init (GtkIEEmbedHistoryItemClass *klass)
{
    GObjectClass   *gobject_class = G_OBJECT_CLASS (klass);

    gobject_class->dispose             = dispose;
    gobject_class->set_property        = set_property;
    gobject_class->get_property        = get_property;

    g_object_class_install_property (
        gobject_class,
        PROP_URI,
        g_param_spec_string (
            "uri",
            "URI",
            "The URI address of the history item",
            NULL,
            G_PARAM_READWRITE | G_PARAM_CONSTRUCT));
    g_object_class_install_property (
        gobject_class,
        PROP_TITLE,
        g_param_spec_string (
            "title",
            "Title",
            "The title of the history item",
            NULL,
            G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

    g_type_class_add_private (gobject_class, sizeof (GtkIEEmbedHistoryItemPriv));
}

static void
gtk_ie_embed_history_item_init (GtkIEEmbedHistoryItem *item)
{
    GtkIEEmbedHistoryItemPriv *priv = GTK_IE_EMBED_HISTORY_ITEM_GET_PRIVATE (item);

    priv->uri = NULL;
    priv->title = NULL;
}

static void
dispose (GObject *object)
{
    GtkIEEmbedHistoryItemPriv *priv = GTK_IE_EMBED_HISTORY_ITEM_GET_PRIVATE (object);

    g_free (priv->uri);
    g_free (priv->title);

    if (G_OBJECT_CLASS (gtk_ie_embed_history_item_parent_class)->dispose)
        G_OBJECT_CLASS (gtk_ie_embed_history_item_parent_class)->dispose (object);
}

static void
set_property (GObject *object,
              guint prop_id,
              const GValue *value,
              GParamSpec *pspec)
{
    switch (prop_id) {
    case PROP_URI:
        gtk_ie_embed_history_item_set_uri (GTK_IE_EMBED_HISTORY_ITEM (object),
                                           g_value_get_string (value));
        break;
    case PROP_TITLE:
        gtk_ie_embed_history_item_set_title (GTK_IE_EMBED_HISTORY_ITEM (object),
                                             g_value_get_string (value));
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

static void
get_property (GObject *object,
              guint prop_id,
              GValue *value,
              GParamSpec *pspec)
{
    GtkIEEmbedHistoryItemPriv *priv = GTK_IE_EMBED_HISTORY_ITEM_GET_PRIVATE (object);

    switch (prop_id) {
    case PROP_URI:
        g_value_set_string (value, priv->uri);
        break;
    case PROP_TITLE:
        g_value_set_string (value, priv->title);
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        break;
    }
}

GtkIEEmbedHistoryItem *
gtk_ie_embed_history_item_new (const gchar *uri, const gchar *title)
{
    return g_object_new (GTK_TYPE_IE_EMBED_HISTORY_ITEM,
                         "uri", uri,
                         "title", title,
                         NULL);
}

const gchar *
gtk_ie_embed_history_item_get_uri (GtkIEEmbedHistoryItem *item)
{
    return GTK_IE_EMBED_HISTORY_ITEM_GET_PRIVATE (item)->uri;
}

const gchar *
gtk_ie_embed_history_item_get_title (GtkIEEmbedHistoryItem *item)
{
    return GTK_IE_EMBED_HISTORY_ITEM_GET_PRIVATE (item)->title;
}

void
gtk_ie_embed_history_item_set_uri (GtkIEEmbedHistoryItem *item, const gchar *uri)
{
    GtkIEEmbedHistoryItemPriv *priv = GTK_IE_EMBED_HISTORY_ITEM_GET_PRIVATE (item);

    g_free (priv->uri);

    priv->uri = g_strdup (uri);
}

void
gtk_ie_embed_history_item_set_title (GtkIEEmbedHistoryItem *item, const gchar *title)
{
    GtkIEEmbedHistoryItemPriv *priv = GTK_IE_EMBED_HISTORY_ITEM_GET_PRIVATE (item);

    g_free (priv->title);

    priv->title = g_strdup (title);
}

/*
vi:ts=4:nowrap:ai:expandtab:sw=4
*/
