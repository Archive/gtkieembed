/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 *  Copyright (C) 2007 Hiroyuki Ikezoe  <poincare@ikezoe.net>
 *
 *  This library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "ie-browser-event-dispatcher.h"

#include <exdisp.h>

IEBrowserEventDispatcher::IEBrowserEventDispatcher (IEBridge *bridge)
{
    mBridge = bridge;
    mRef = 0;
    mReady = FALSE;
}

IEBrowserEventDispatcher::~IEBrowserEventDispatcher ()
{
    mBridge = 0;
}

HRESULT __stdcall
IEBrowserEventDispatcher::QueryInterface (REFIID riid, void **ppvObject)
{
    if (ppvObject == 0)
        return E_NOINTERFACE;

    if (IsEqualIID (riid, IID_IUnknown) ||
        IsEqualIID (riid, IID_IDispatch) ||
        IsEqualIID (riid, DIID_DWebBrowserEvents2)) {
        *ppvObject = (IUnknown *) this;
        return S_OK;
    }

    *ppvObject = 0;

    return E_NOINTERFACE;
}

ULONG __stdcall
IEBrowserEventDispatcher::AddRef ()
{
    return ++mRef;
}

ULONG __stdcall
IEBrowserEventDispatcher::Release ()
{
    return --mRef;
}

HRESULT __stdcall
IEBrowserEventDispatcher::GetTypeInfoCount (UINT*)
{
    return E_NOTIMPL;
}

HRESULT __stdcall
IEBrowserEventDispatcher::GetTypeInfo (UINT,LCID,LPTYPEINFO*)
{
    return E_NOTIMPL;
}

HRESULT __stdcall
IEBrowserEventDispatcher::GetIDsOfNames (REFIID,LPOLESTR*,UINT,LCID,DISPID*)
{
    return E_NOTIMPL;
}

#define DISPID_BEFORENAVIGATE        100
#define DISPID_NAVIGATECOMPLETE      101
#define DISPID_STATUSTEXTCHANGE      102
#define DISPID_QUIT                  103
#define DISPID_DOWNLOADCOMPLETE      104
#define DISPID_COMMANDSTATECHANGE    105
#define DISPID_DOWNLOADBEGIN         106
#define DISPID_NEWWINDOW             107
#define DISPID_PROGRESSCHANGE        108
#define DISPID_WINDOWMOVE            109
#define DISPID_WINDOWRESIZE          110
#define DISPID_WINDOWACTIVATE        111
#define DISPID_PROPERTYCHANGE        112
#define DISPID_TITLECHANGE           113
#define DISPID_TITLEICONCHANGE       114

#define DISPID_FRAMEBEFORENAVIGATE   200
#define DISPID_FRAMENAVIGATECOMPLETE 201

#define DISPID_FRAMENEWWINDOW        204

#define DISPID_PRINTTEMPLATEINSTANTIATION 225
#define DISPID_PRINTTEMPLATETEARDOWN      226
#define DISPID_UPDATEPAGESTATUS           227

#define DISPID_BEFORENAVIGATE2       250
#define DISPID_NEWWINDOW2            251
#define DISPID_NAVIGATECOMPLETE2     252
#define DISPID_ONQUIT                253
#define DISPID_ONVISIBLE             254
#define DISPID_ONTOOLBAR             255
#define DISPID_ONMENUBAR             256
#define DISPID_ONSTATUSBAR           257
#define DISPID_ONFULLSCREEN          258
#define DISPID_DOCUMENTCOMPLETE      259
#define DISPID_ONTHEATERMODE         260
#define DISPID_ONADDRESSBAR          261
#define DISPID_WINDOWSETRESIZABLE    262
#define DISPID_WINDOWCLOSING         263
#define DISPID_WINDOWSETLEFT         264
#define DISPID_WINDOWSETTOP          265
#define DISPID_WINDOWSETWIDTH        266
#define DISPID_WINDOWSETHEIGHT       267
#define DISPID_CLIENTTOHOSTWINDOW    268
#define DISPID_SETSECURELOCKICON     269
#define DISPID_FILEDOWNLOAD          270
#define DISPID_NAVIGATEERROR         271
#define DISPID_PRIVACYIMPACTEDSTATECHANGE 272

HRESULT __stdcall
IEBrowserEventDispatcher::Invoke (DISPID dispIdMember,
                                  REFIID riid,
                                  LCID lcid,
                                  WORD wFlags,
                                  DISPPARAMS* pDispParams, 
                                  VARIANT* pVarResult,
                                  EXCEPINFO* pExcepInfo,
                                  UINT* puArgErr)
{
    switch (dispIdMember) {
     case DISPID_BEFORENAVIGATE2:
        BeforeNavigate2(pDispParams->rgvarg[6].pdispVal,
                        pDispParams->rgvarg[5].pvarVal,
                        pDispParams->rgvarg[4].pvarVal,
                        pDispParams->rgvarg[3].pvarVal,
                        pDispParams->rgvarg[2].pvarVal,
                        pDispParams->rgvarg[1].pvarVal,
                        pDispParams->rgvarg[0].pboolVal);
        break;
     case DISPID_NEWWINDOW2:
        NewWindow2 (pDispParams->rgvarg[1].ppdispVal, pDispParams->rgvarg[0].pboolVal);
        break;
     case DISPID_PROGRESSCHANGE:
        ProgressChange (pDispParams->rgvarg[1].lVal, pDispParams->rgvarg[0].lVal);
        break;
     case DISPID_DOCUMENTCOMPLETE:
        DocumentComplete (pDispParams->rgvarg[1].pdispVal, pDispParams->rgvarg[0].pvarVal);
        break;
     case DISPID_TITLECHANGE:
        TitleChange (pDispParams->rgvarg[0].bstrVal);
        break;
     case DISPID_STATUSTEXTCHANGE:
        StatusTextChange (pDispParams->rgvarg[0].bstrVal);
        break;
     case DISPID_COMMANDSTATECHANGE:
        CommandStateChange (pDispParams->rgvarg[1].lVal, pDispParams->rgvarg[0].boolVal);
        break;
     case DISPID_NAVIGATECOMPLETE2:
        NavigateComplete2 (pDispParams->rgvarg[1].pdispVal, pDispParams->rgvarg[0].pvarVal);
        break;
     case DISPID_WINDOWCLOSING:
        WindowClosing (pDispParams->rgvarg[1].boolVal, pDispParams->rgvarg[0].pboolVal);
        break;
     case DISPID_DOWNLOADBEGIN:
        DownloadBegin ();
        break;
     case DISPID_DOWNLOADCOMPLETE:
        DownloadComplete ();
        break;
     case DISPID_SETSECURELOCKICON:
        SetSecureLockIcon (pDispParams->rgvarg[0].lVal);
        break;
     case DISPID_PROPERTYCHANGE:
        PropertyChange (pDispParams->rgvarg[0].bstrVal);
        break;
     case DISPID_FILEDOWNLOAD:
        FileDownload (pDispParams->rgvarg[0].pboolVal);
        break;
     case DISPID_PRIVACYIMPACTEDSTATECHANGE:
        PrivacyImpactedStateChange (pDispParams->rgvarg[0].boolVal);
        break;
     case DISPID_PRINTTEMPLATEINSTANTIATION:
     case DISPID_PRINTTEMPLATETEARDOWN:
     case DISPID_UPDATEPAGESTATUS:
     case DISPID_NAVIGATEERROR:
        break;
     case DISPID_ONVISIBLE:
        OnVisible (pDispParams->rgvarg[0].boolVal);
        break;
     default:
        break;
    }

    return S_OK;
}

void
IEBrowserEventDispatcher::StatusTextChange (BSTR Text)
{
    _ie_bridge_status_text_change (mBridge, Text);
}

void
IEBrowserEventDispatcher::ProgressChange (long lProgress, long lProgressMax)
{
    _ie_bridge_progress_change (mBridge, lProgress, lProgressMax);
}

void
IEBrowserEventDispatcher::CommandStateChange (long lCommand, VARIANT_BOOL vbEnable)
{
    switch (lCommand) {
     case 0x01: /* CSC_NAVIGATE_FORWARD */
        _ie_bridge_set_forward_state (mBridge, vbEnable);
        break;
     case 0x02: /* CSC_NAVIGATE_BACKWORD */
        _ie_bridge_set_backward_state (mBridge, vbEnable);
        break;
    }
}

void
IEBrowserEventDispatcher::DownloadBegin ()
{
}

void
IEBrowserEventDispatcher::DownloadComplete ()
{
}

void
IEBrowserEventDispatcher::TitleChange (BSTR Title)
{
    _ie_bridge_title_changed (mBridge, Title);
}

void
IEBrowserEventDispatcher::PropertyChange (BSTR)
{
}

void
IEBrowserEventDispatcher::BeforeNavigate2 (IDispatch *pDisp,
                                           VARIANT *URL,
                                           VARIANT *pvtFlags,
                                           VARIANT *pvtTargetFrameName,
                                           VARIANT *pvtPostData,
                                           VARIANT *pvtHeaders,
                                           VARIANT_BOOL *pvbCancel)
{
    _ie_bridge_net_start (mBridge);
    _ie_bridge_disconnect_document_event_dispatcher (mBridge);
}

void
IEBrowserEventDispatcher::NewWindow2 (IDispatch** ppDisp, VARIANT_BOOL *pvbCancel)
{
    *pvbCancel = _ie_bridge_new_window (mBridge, (void **) ppDisp) ? VARIANT_TRUE : VARIANT_FALSE;
}

void
IEBrowserEventDispatcher::NavigateComplete2 (IDispatch *pDisp, VARIANT *URL)
{
    if (pDisp == _ie_bridge_get_browser_object (mBridge))
        _ie_bridge_location_changed (mBridge, URL->bstrVal);
}

void
IEBrowserEventDispatcher::DocumentComplete (IDispatch *wbDisp, VARIANT *URL)
{
    /* Check wbDisp is not framed page */
    if (wbDisp == _ie_bridge_get_browser_object (mBridge)) {
        _ie_bridge_connect_document_event_dispatcher (mBridge);
        _ie_bridge_net_stop (mBridge);
    }
    if (!mReady) {
        mReady = TRUE;
        _ie_bridge_ready (mBridge);
    }
}

void
IEBrowserEventDispatcher::WindowClosing (VARIANT_BOOL IsChildWindow, VARIANT_BOOL *pCancel)
{
    *pCancel = _ie_bridge_close_window (mBridge, IsChildWindow) ? VARIANT_TRUE : VARIANT_FALSE;
}

void
IEBrowserEventDispatcher::ClientToHostWindow (long*,long*)
{
}

void
IEBrowserEventDispatcher::SetSecureLockIcon (long)
{
}

void
IEBrowserEventDispatcher::FileDownload (VARIANT_BOOL*)
{
}

void
IEBrowserEventDispatcher::PrivacyImpactedStateChange (VARIANT_BOOL)
{
}

void
IEBrowserEventDispatcher::OnVisible (VARIANT_BOOL)
{
}

/*
vi:ts=4:nowrap:ai:expandtab:sw=4
*/
