/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 *  Copyright (C) 2009 Hiroyuki Ikezoe  <poincare@ikezoe.net>
 *
 *  This library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __IE_UTILS__
#define __IE_UTILS__

#include <glib.h>
#include <windows.h>

G_BEGIN_DECLS

gchar *_ie_utils_BSTR_to_utf8 (BSTR bstr);
BSTR   _ie_utils_utf8_to_BSTR (const gchar *str);

G_END_DECLS

#endif /* __IE_UTILS__ */
/*
vi:ts=4:nowrap:ai:expandtab:sw=4
*/
