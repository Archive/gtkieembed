#include "gtk-ie-embed.h"

static GtkWidget *statusbar;
static GtkWidget *entry;
static GtkActionGroup *action_group;

static GtkIEEmbed *create_new_embed (GtkNotebook *notebook);

static GtkIEEmbed *
get_current_embed (GtkNotebook *notebook)
{
    gint page_num = gtk_notebook_get_current_page (notebook);
    return GTK_IE_EMBED (gtk_notebook_get_nth_page (notebook, page_num));
}

static void
open_new_tab_action (GtkAction *action, GtkNotebook *notebook)
{
    create_new_embed (GTK_NOTEBOOK (notebook));
}

static void
close_tab_action (GtkAction *action, GtkNotebook *notebook)
{
    GtkIEEmbed *ie = get_current_embed (notebook);
    if (!ie)
        return;
    gtk_widget_destroy (GTK_WIDGET (ie));
}

static void
cut_action (GtkAction *action, GtkNotebook *notebook)
{
    GtkIEEmbed *ie = get_current_embed (notebook);
    gtk_ie_embed_cut_clipboard (ie);
}

static void
copy_action (GtkAction *action, GtkNotebook *notebook)
{
    GtkIEEmbed *ie = get_current_embed (notebook);
    gtk_ie_embed_copy_clipboard (ie);
}

static void
paste_action (GtkAction *action, GtkNotebook *notebook)
{
    GtkIEEmbed *ie = get_current_embed (notebook);
    gtk_ie_embed_paste_clipboard (ie);
}

static void
select_all_action (GtkAction *action, GtkNotebook *notebook)
{
    GtkIEEmbed *ie = get_current_embed (notebook);
    gtk_ie_embed_select_all (ie);
}

static void
find_string_action (GtkAction *action, GtkNotebook *notebook)
{
    GtkWidget *dialog, *parent_window, *entry;
    GtkIEEmbed *ie = get_current_embed (notebook);

    parent_window = gtk_widget_get_toplevel (GTK_WIDGET (notebook));
    dialog = gtk_dialog_new_with_buttons ("Find string",
                                          GTK_WINDOW (parent_window),
                                          GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
                                          GTK_STOCK_OK,
                                          GTK_RESPONSE_ACCEPT,
                                          GTK_STOCK_CANCEL,
                                          GTK_RESPONSE_REJECT,
                                          NULL);
    gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_ACCEPT);
    entry = gtk_entry_new ();
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), entry, TRUE, TRUE, 0);
    gtk_widget_show (entry);
    while (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT) {
        const gchar *string;
        string = gtk_entry_get_text (GTK_ENTRY (entry));
        gtk_ie_embed_find_string (ie, string, TRUE, TRUE);
    }

    gtk_widget_destroy (dialog);
}

static void
forward_action (GtkAction *action, GtkNotebook *notebook)
{
    GtkIEEmbed *ie = get_current_embed (notebook);
    gtk_ie_embed_go_forward (ie);
}

static void
back_action (GtkAction *action, GtkNotebook *notebook)
{
    GtkIEEmbed *ie = get_current_embed (notebook);
    gtk_ie_embed_go_back (ie);
}

static void
stop_action (GtkAction *action, GtkNotebook *notebook)
{
    GtkIEEmbed *ie = get_current_embed (notebook);
    gtk_ie_embed_stop (ie);
}

static void
reload_action (GtkAction *action, GtkNotebook *notebook)
{
    GtkIEEmbed *ie = get_current_embed (notebook);
    gtk_ie_embed_reload (ie, GTK_IE_EMBED_RELOAD_NORMAL);
}

static void
font_size_action (GtkRadioAction *action, GtkRadioAction *cur_action, GtkNotebook *notebook)
{
    gint font_size;
    GtkIEEmbed *ie = get_current_embed (notebook);
    font_size = gtk_radio_action_get_current_value (GTK_RADIO_ACTION (action));
    gtk_ie_embed_set_font_size (ie, font_size);
}

static void
print_action (GtkAction *action, GtkNotebook *notebook)
{
    GtkIEEmbed *ie = get_current_embed (notebook);
    gtk_ie_embed_print (ie);
}

static void
print_preview_action (GtkAction *action, GtkNotebook *notebook)
{
    GtkIEEmbed *ie = get_current_embed (notebook);
    gtk_ie_embed_print_preview (ie);
}

static void
page_setup_action (GtkAction *action, GtkNotebook *notebook)
{
    GtkIEEmbed *ie = get_current_embed (notebook);
    gtk_ie_embed_page_setup (ie);
}

static void
save_as_action (GtkAction *action, GtkNotebook *notebook)
{
    GtkWidget *dialog;
    GtkWidget *parent_window;
    GtkIEEmbed *ie = get_current_embed (notebook);

    parent_window = gtk_widget_get_toplevel (GTK_WIDGET (notebook));
    dialog = gtk_file_chooser_dialog_new ("Save File",
                                          GTK_WINDOW (parent_window),
                                          GTK_FILE_CHOOSER_ACTION_SAVE,
                                          GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                          GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
                                          NULL);
    gtk_file_chooser_set_do_overwrite_confirmation (GTK_FILE_CHOOSER (dialog), TRUE);
    if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_OK) {
        gchar *filename;
        filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
        gtk_ie_embed_save_as (ie, filename);
        g_free (filename);
    }
    gtk_widget_destroy (dialog);
}

static void
use_context_menu_action (GtkAction *action, GtkNotebook *notebook)
{
	gboolean active;
    GtkIEEmbed *ie = get_current_embed (notebook);

	active = gtk_toggle_action_get_active (GTK_TOGGLE_ACTION(action));
    gtk_ie_embed_set_use_context_menu (ie, active);
}

static GtkActionEntry entries[] = {
    { "FileAction", NULL, "File" },
    { "EditAction", NULL, "Edit" },
    { "ViewAction", NULL, "View" },

    { "open-new-tab", GTK_STOCK_NEW, "Open new _Tab", "<control>T",
        "Open a new tab", G_CALLBACK (open_new_tab_action) },
    { "close-tab", GTK_STOCK_CLOSE, "_Close Current Tab", "<control>W",
        "Close current tab", G_CALLBACK (close_tab_action) },
    { "page-setup", NULL,  "Page Set_up", NULL,
        "Page setup", G_CALLBACK (page_setup_action) },
    { "print", GTK_STOCK_PRINT,  NULL, "<control>P",
        "Print this page", G_CALLBACK (print_action) },
    { "print-preview", GTK_STOCK_PRINT_PREVIEW,  NULL, "<shift><control>P",
        "Preview this page", G_CALLBACK (print_preview_action) },
    { "save-as", GTK_STOCK_SAVE_AS,  NULL, "<control>S",
        "Save this page", G_CALLBACK (save_as_action) },
    { "quit", GTK_STOCK_QUIT, NULL, "<control>Q",
        "Quit the application", G_CALLBACK (gtk_main_quit) },
    { "cut", GTK_STOCK_CUT, NULL, "<control>X",
        "Cut the selected text to the clipboard", G_CALLBACK (cut_action) },
    { "copy", GTK_STOCK_COPY, NULL, "<control>C",
        "Copy the selected text to the clipboard", G_CALLBACK (copy_action) },
    { "paste", GTK_STOCK_PASTE, NULL, "<control>V",
        "Paste the text from the clipboard", G_CALLBACK (paste_action) },
    { "select-all", GTK_STOCK_SELECT_ALL, NULL, "<control>A",
        "Select all text", G_CALLBACK (select_all_action) },
    { "find-string", GTK_STOCK_FIND, NULL, "<control>F",
        "Find string", G_CALLBACK (find_string_action) },
    { "back", GTK_STOCK_GO_BACK, NULL, "<alt>Left",
        "Go to previous page", G_CALLBACK (back_action) },
    { "forward", GTK_STOCK_GO_FORWARD, NULL, "<alt>Right",
        "Go to next page", G_CALLBACK (forward_action) },
    { "stop", GTK_STOCK_STOP, NULL, "<control>S",
        "Stop loading", G_CALLBACK (stop_action) },
    { "reload", GTK_STOCK_REFRESH, NULL, "<control>R",
        "Reload page", G_CALLBACK (reload_action) }

};
static guint n_entries = G_N_ELEMENTS (entries);

static GtkRadioActionEntry font_size_entries[] = {
    { "font-size-largest", NULL, "Font Size Lar_gest", NULL,
        "Font size is largest", GTK_IE_EMBED_FONT_LARGEST },
    { "font-size-large", NULL, "Font Size _Large", NULL,
        "Font size is large", GTK_IE_EMBED_FONT_LARGE },
    { "font-size-medium", NULL, "Font Size _Meduim", NULL,
        "Font size is medium", GTK_IE_EMBED_FONT_MEDIUM },
    { "font-size-small", NULL, "Font Size _Small", NULL,
        "Font size is small", GTK_IE_EMBED_FONT_SMALL },
    { "font-size-smallest", NULL, "Font Size Small_est", NULL,
        "Font size is smallest", GTK_IE_EMBED_FONT_SMALLEST }
};
static guint n_font_size_entries = G_N_ELEMENTS (font_size_entries);

static GtkToggleActionEntry toggle_entries[] =
{
    { "use-context-menu", NULL, "Use Context _Menu", "<control>M",
        "Use original context menu", G_CALLBACK (use_context_menu_action) }
};
static const gint n_toggle_entries = G_N_ELEMENTS(toggle_entries);

static const gchar *ui_info =
"  <menubar>\n"
"    <menu name=\"File\" action=\"FileAction\">\n"
"      <menuitem name=\"open-new-tab\" action=\"open-new-tab\" />\n"
"      <menuitem name=\"close-tab\" action=\"close-tab\" />\n"
"      <separator name=\"file-sep1\" />\n"
"      <menuitem name=\"page-setup\" action=\"page-setup\" />\n"
"      <menuitem name=\"print\" action=\"print\" />\n"
"      <menuitem name=\"print-preview\" action=\"print-preview\" />\n"
"      <separator name=\"file-sep2\" />\n"
"      <menuitem name=\"save-as\" action=\"save-as\" />\n"
"      <separator name=\"file-sep3\" />\n"
"      <menuitem name=\"quit\" action=\"quit\" />\n"
"    </menu>\n"
"    <menu name=\"Edit\" action=\"EditAction\">\n"
"      <menuitem name=\"cut\" action=\"cut\" />\n"
"      <menuitem name=\"copy\" action=\"copy\" />\n"
"      <menuitem name=\"paste\" action=\"paste\" />\n"
"      <separator name=\"edit-sep1\" />\n"
"      <menuitem name=\"select-all\" action=\"select-all\" />\n"
"      <separator name=\"edit-sep2\" />\n"
"      <menuitem name=\"find-string\" action=\"find-string\" />\n"
"    </menu>\n"
"    <menu name=\"View\" action=\"ViewAction\">\n"
"      <menuitem name=\"font-size-largest\" action=\"font-size-largest\" />\n"
"      <menuitem name=\"font-size-large\" action=\"font-size-large\" />\n"
"      <menuitem name=\"font-size-medium\" action=\"font-size-medium\" />\n"
"      <menuitem name=\"font-size-small\" action=\"font-size-small\" />\n"
"      <menuitem name=\"font-size-smallest\" action=\"font-size-smallest\" />\n"
"      <separator name=\"view-sep1\" />\n"
"      <menuitem name=\"use-context-menu\" action=\"use-context-menu\" />\n"
"    </menu>\n"
"  </menubar>\n"
"  <toolbar name=\"toolbar\">\n"
"    <toolitem name=\"back\" action=\"back\" />\n"
"    <toolitem name=\"forward\" action=\"forward\" />\n"
"    <toolitem name=\"stop\" action=\"stop\" />\n"
"    <toolitem name=\"reload\" action=\"reload\" />\n"
"  </toolbar>\n";

static gboolean
cb_delete_event (GtkWidget *widget)
{
    gtk_main_quit ();

    return FALSE;
}

static gboolean
cb_entry_button_release (GtkWidget *widget,
                         GdkEventButton *event,
                         GtkNotebook *notebook)
{
    gdk_window_focus (widget->window, GDK_CURRENT_TIME);
    return FALSE;
}

static void
cb_entry_activate (GtkEntry *entry, GtkNotebook *notebook)
{
    const gchar *text;
    GtkIEEmbed *ie = get_current_embed (notebook);

    text = gtk_entry_get_text (entry);
    gtk_ie_embed_load_url (ie, text);
}

static void
cb_title_changed (GtkIEEmbed *ie, const gchar *title, GtkWidget *notebook)
{
    GtkWidget *label;

    label = gtk_notebook_get_tab_label (GTK_NOTEBOOK (notebook),
                                        GTK_WIDGET (ie));

    gtk_label_set_text (GTK_LABEL (label), title);
}

static void
cb_location_changed (GtkIEEmbed *ie, const gchar *location, GtkWidget *entry)
{
    gtk_entry_set_text (GTK_ENTRY (entry), location);
}

static void
cb_status_text_changed (GtkIEEmbed *ie, const gchar *text, GtkWidget *statusbar)
{
    guint id;

    id = gtk_statusbar_get_context_id (GTK_STATUSBAR (statusbar), "STATUSTEXT");
    gtk_statusbar_pop (GTK_STATUSBAR (statusbar), id);
    gtk_statusbar_push (GTK_STATUSBAR (statusbar), id, text);
}

static void
set_navigate_actions_sensitivity (GtkIEEmbed *ie)
{
    GtkAction *action;
    gboolean sensitivity;

    action = gtk_action_group_get_action (action_group, "back");
    sensitivity = gtk_ie_embed_can_go_back (ie);
    gtk_action_set_sensitive (action, sensitivity);

    action = gtk_action_group_get_action (action_group, "forward");
    sensitivity = gtk_ie_embed_can_go_forward (ie);
    gtk_action_set_sensitive (action, sensitivity);
}

static void
set_use_context_menu_action_condition (GtkIEEmbed *ie)
{
    GtkAction *action;
    gboolean condition;

    action = gtk_action_group_get_action (action_group, "use-context-menu");
    condition = gtk_ie_embed_get_use_context_menu (ie);
    gtk_toggle_action_set_active (GTK_TOGGLE_ACTION (action), condition);
}

static void
set_edit_actions_sensitivity (GtkIEEmbed *ie)
{
    GtkAction *action;
    gboolean sensitivity;

    action = gtk_action_group_get_action (action_group, "cut");
    sensitivity = gtk_ie_embed_can_cut_clipboard (ie);
    gtk_action_set_sensitive (action, sensitivity);

    action = gtk_action_group_get_action (action_group, "copy");
    sensitivity = gtk_ie_embed_can_copy_clipboard (ie);
    gtk_action_set_sensitive (action, sensitivity);

    action = gtk_action_group_get_action (action_group, "paste");
    sensitivity = gtk_ie_embed_can_paste_clipboard (ie);
    gtk_action_set_sensitive (action, sensitivity);
}

static void
cb_net_stop (GtkIEEmbed *ie, gpointer data)
{
    set_navigate_actions_sensitivity (ie);
}

static gboolean
cb_new_window (GtkIEEmbed *ie, GtkIEEmbed **newie, GtkWidget *notebook)
{
    *newie = create_new_embed (GTK_NOTEBOOK (notebook));

    return FALSE;
}

static gboolean
cb_close_window (GtkIEEmbed *ie, GtkWidget *window)
{
    gtk_widget_destroy (GTK_WIDGET (ie));
    return FALSE;
}

static void
cb_selection_changed (GtkIEEmbed *ie)
{
    set_edit_actions_sensitivity (ie);
}

static void
print_dom_mouse_event (GtkIEEmbedDOMMouseEvent *event, const gchar *mouse_event_name)
{
    if (event->target)
        g_print ("DOM mouse %s on %s.\n", mouse_event_name,
                gtk_ie_embed_dom_event_target_get_name(event->target));
    else
        g_print ("DOM mouse %s.\n", mouse_event_name);
}

static gboolean
cb_dom_mouse_down (GtkIEEmbed *ie, GtkIEEmbedDOMMouseEvent *event)
{
    print_dom_mouse_event (event, "down");
    return FALSE;
}

static gboolean
cb_dom_mouse_move (GtkIEEmbed *ie, GtkIEEmbedDOMMouseEvent *event)
{
    print_dom_mouse_event (event, "move");
    return FALSE;
}

static gboolean
cb_dom_mouse_up (GtkIEEmbed *ie, GtkIEEmbedDOMMouseEvent *event)
{
    print_dom_mouse_event (event, "up");
    return FALSE;
}

static gboolean
cb_dom_mouse_click (GtkIEEmbed *ie, GtkIEEmbedDOMMouseEvent *event)
{
    print_dom_mouse_event (event, "click");
    return FALSE;
}

static void
add_widget (GtkUIManager *merge, GtkWidget *widget, GtkContainer *container)
{
    gtk_box_pack_start (GTK_BOX (container), widget, FALSE, FALSE, 0);
    gtk_widget_show (widget);
}

static void
cb_switch_page (GtkNotebook *notebook, GtkNotebookPage *page,
                guint page_num, GtkWidget *window)
{
    GtkIEEmbed *embed;

    embed = get_current_embed (notebook);
    if (embed) {
        g_signal_handlers_disconnect_by_func (embed,
                G_CALLBACK (cb_location_changed),
                entry);
        g_signal_handlers_disconnect_by_func (embed,
                G_CALLBACK (cb_status_text_changed),
                statusbar);
        g_signal_handlers_disconnect_by_func (embed,
                G_CALLBACK (cb_net_stop),
                NULL);
        g_signal_handlers_disconnect_by_func (embed,
                G_CALLBACK (cb_selection_changed),
                NULL);
    }

    embed = GTK_IE_EMBED (gtk_notebook_get_nth_page (notebook, page_num));
    if (embed) {
        gchar *location;
        g_signal_connect (embed, "location", 
                G_CALLBACK (cb_location_changed), entry);
        g_signal_connect (embed, "status-text", 
                G_CALLBACK (cb_status_text_changed), statusbar);
        g_signal_connect (embed, "net-stop",
                G_CALLBACK (cb_net_stop), NULL);
        g_signal_connect (embed, "selection-changed",
                G_CALLBACK (cb_selection_changed), NULL);
        g_signal_connect (embed, "dom-mouse-down",
                G_CALLBACK (cb_dom_mouse_down), NULL);
        g_signal_connect (embed, "dom-mouse-up",
                G_CALLBACK (cb_dom_mouse_up), NULL);
        g_signal_connect (embed, "dom-mouse-move",
                G_CALLBACK (cb_dom_mouse_move), NULL);
        g_signal_connect (embed, "dom-mouse-click",
                G_CALLBACK (cb_dom_mouse_click), NULL);
        location = gtk_ie_embed_get_location (embed);
        if (location) {
            gtk_entry_set_text (GTK_ENTRY (entry), location);
            g_free (location);
        }
        set_navigate_actions_sensitivity (embed);
        set_use_context_menu_action_condition (embed);
    }
}

static GtkIEEmbed *
create_new_embed (GtkNotebook *notebook)
{
    GtkWidget *embed, *label;

    embed = gtk_ie_embed_new ();
    label = gtk_label_new (NULL);
    gtk_widget_show (embed);
    gtk_widget_show (label);
    gtk_notebook_append_page (GTK_NOTEBOOK (notebook), embed, label);

    g_signal_connect (embed, "title", 
                      G_CALLBACK (cb_title_changed), notebook);
    g_signal_connect (embed, "new-window", G_CALLBACK (cb_new_window), notebook);
    g_signal_connect (embed, "close-window", G_CALLBACK (cb_close_window), notebook);

    return GTK_IE_EMBED (embed);
}

static void
append_new_embed_with_url (GtkNotebook *notebook, const gchar *url)
{
    GtkIEEmbed *embed;

    embed = create_new_embed (GTK_NOTEBOOK (notebook));
    gtk_ie_embed_load_url (embed, url);
}

static void
append_new_embed_with_history (GtkNotebook *notebook, GList *history)
{
    GtkIEEmbed *embed;

    embed = create_new_embed (GTK_NOTEBOOK (notebook));
    gtk_ie_embed_set_whole_history (embed, history, g_list_length (history));
}

static void
append_new_welcome_embed (GtkNotebook *notebook)
{
    GtkIEEmbed *embed;
    gchar contents[] = "<html><title>Welcome</title>"
                       "<body>This page is rendered by GtkIEEmbed.</body>"
                       "</html>";

    embed = create_new_embed (GTK_NOTEBOOK (notebook));

    gtk_ie_embed_load_html_from_string (embed, contents);
}

static void
create_main_window (void)
{
    GtkWidget *window;
    GtkWidget *notebook;
    GtkWidget *vbox, *hbox;
    GtkUIManager *merge;
    GList *history = NULL;

    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_window_set_default_size (GTK_WINDOW (window), 600, 400);
    g_signal_connect (window, "delete-event",
            G_CALLBACK (cb_delete_event), window);
    vbox = gtk_vbox_new (FALSE, 0);
    gtk_container_add (GTK_CONTAINER (window), vbox);
    gtk_widget_show (vbox);

    hbox = gtk_hbox_new (FALSE, 0);
    gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
    gtk_widget_show (hbox);

    entry = gtk_entry_new ();
    gtk_box_pack_start (GTK_BOX (hbox), entry, TRUE, TRUE, 0);
    gtk_widget_show (entry);

    notebook = gtk_notebook_new ();
    gtk_notebook_set_scrollable(GTK_NOTEBOOK(notebook), TRUE);

    action_group = gtk_action_group_new ("TestActions");
    gtk_action_group_add_actions (action_group,
                                  entries, n_entries,
                                  GTK_NOTEBOOK (notebook));
    gtk_action_group_add_radio_actions (action_group, 
                                        font_size_entries,
                                        n_font_size_entries, 
                                        GTK_IE_EMBED_FONT_MEDIUM,
                                        G_CALLBACK (font_size_action),
                                        GTK_NOTEBOOK (notebook));
	gtk_action_group_add_toggle_actions (action_group,
                                         toggle_entries,
                                         n_toggle_entries,
                                         GTK_NOTEBOOK (notebook));
    merge = gtk_ui_manager_new ();
    gtk_ui_manager_insert_action_group (merge, action_group, 0);
    g_signal_connect (merge, "add_widget", G_CALLBACK (add_widget), vbox);
    gtk_window_add_accel_group (GTK_WINDOW (window), 
                                gtk_ui_manager_get_accel_group (merge));
    gtk_ui_manager_add_ui_from_string (merge, ui_info, -1, NULL);
    gtk_ui_manager_ensure_update (merge);

    gtk_box_pack_start (GTK_BOX (vbox), notebook, TRUE, TRUE, 0);
    gtk_widget_show (notebook);

    statusbar = gtk_statusbar_new ();
    gtk_box_pack_end (GTK_BOX (vbox), statusbar, FALSE, FALSE, 0);
    gtk_widget_show (statusbar);

    g_signal_connect (notebook, "switch-page",
                      G_CALLBACK (cb_switch_page), window);
    g_signal_connect (entry, "activate",
                      G_CALLBACK(cb_entry_activate), notebook);
    g_signal_connect (entry, "button-release-event",
                      G_CALLBACK(cb_entry_button_release), notebook);

    gtk_widget_show (window);

    append_new_welcome_embed (GTK_NOTEBOOK (notebook));
    append_new_embed_with_url (GTK_NOTEBOOK (notebook),
                               "http://www.gnome.org/");
    history = g_list_append (history, 
                             gtk_ie_embed_history_item_new ("http://www.google.com/", "1st page"));
    history = g_list_append (history, 
                             gtk_ie_embed_history_item_new ("http://www.google.com/search?q=gonme", "2nd page"));
    history = g_list_append (history, 
                             gtk_ie_embed_history_item_new ("http://www.gnome.org/", "3rd page"));
    history = g_list_append (history, 
                             gtk_ie_embed_history_item_new ("http://www.gnome.org/start/stable/", "4th page"));
    append_new_embed_with_history (GTK_NOTEBOOK (notebook), history);

    g_list_foreach (history, (GFunc) g_object_unref, NULL);
    g_list_free (history);
}

int
main (int argc, char *argv[])
{
    gtk_init (&argc, &argv);

    create_main_window ();

    gtk_main ();

    g_object_unref (action_group);

    return 0;
}

#ifdef _WINDOWS
#include <windows.h>
int WINAPI
WinMain (HINSTANCE hInstance,
         HINSTANCE hPrevInstance,
         LPSTR     lpszCmdLine,
         int       nCmdShow)
{
    char **argv;
    int i, argc, ret = -1;

    argc = __argc;
    argv = g_malloc ((argc + 1) * sizeof (char *));
    for (i = 0; i < argc; i++)
        argv[i] = g_locale_to_utf8 (__argv[i], -1, NULL, NULL, NULL);
    argv[i] = NULL;
    ret = main (argc, argv);
    for (i = 0; i < argc; i++)
        g_free (argv[i]);
    g_free (argv);
    return ret;
}
#endif /* _WINDOWS */

/*
vi:ts=4:nowrap:ai:expandtab:sw=4
*/
